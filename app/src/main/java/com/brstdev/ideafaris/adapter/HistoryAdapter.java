package com.brstdev.ideafaris.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brstdev.ideafaris.R;
import com.brstdev.ideafaris.activity.StoryDetailsActivity;
import com.brstdev.ideafaris.model.History;
import com.brstdev.ideafaris.model.RecentStory;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by brst-pc16 on 9/13/16.
 */
public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder> {

    private List<History> histories;
    public int isClicked =  0;
    public int isClicked1 =  0;
    public Context mContext;
    public class MyViewHolder extends RecyclerView.ViewHolder {

        public CircleImageView imageView;
        public TextView title, year, genre,timer,dislike,like;
        public ImageView likethump,unlikethump;
        private RelativeLayout layRel;

        public MyViewHolder(View view) {
            super(view);
            imageView = (CircleImageView) view.findViewById(R.id.title);
            genre = (TextView) view.findViewById(R.id.genre);
            year = (TextView) view.findViewById(R.id.year);
            timer=(TextView) view.findViewById(R.id.timer);
            dislike=(TextView)view.findViewById(R.id.dislike);
            like=(TextView)view.findViewById(R.id.like);
            likethump=(ImageView)view.findViewById(R.id.likethump);
            unlikethump=(ImageView)view.findViewById(R.id.unlikethump);
            layRel=(RelativeLayout)view.findViewById(R.id.layRel);
        }
    }
    public HistoryAdapter(List<History> histories,Context mContext) {
        this.histories = histories;
        this.mContext=mContext;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.history_item, parent, false);
        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        History history = histories.get(position);
        holder.setIsRecyclable(false);
        holder.genre.setText(history.getGenre());
        holder.year.setText(history.getYear());
        holder.timer.setText(history.getTime());
        holder.like.setText(history.getLike());
        holder.dislike.setText(history.getDisLike());
        if(!history.getImageUrl().contains("http"))
        {
            try {
                byte [] encodeByte= Base64.decode(history.getImageUrl(), Base64.DEFAULT);
                Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
                holder.imageView.setImageBitmap(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Picasso.with(mContext).load(history.getImageUrl()).into(holder.imageView);
        }
        holder.layRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                //final RecentStory story = recentStories.get(postion).getStoryId();
                Intent i=new Intent(mContext, StoryDetailsActivity.class).putExtra("storyId", histories.get(position).getStoryId()).putExtra("title", histories.get(position).getYear().toString());
                mContext.startActivity(i);

            }
        });
    }

    @Override
    public int getItemCount() {
        return histories.size();
    }
}
