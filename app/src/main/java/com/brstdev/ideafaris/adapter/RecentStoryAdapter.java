package com.brstdev.ideafaris.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brstdev.ideafaris.R;
import com.brstdev.ideafaris.activity.StoryDetailsActivity;
import com.brstdev.ideafaris.fragment.RecentStoryFragment;
import com.brstdev.ideafaris.model.RecentStory;
import com.brstdev.ideafaris.model.TopRated;
import com.brstdev.ideafaris.utils.IoUpdateCallback;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by brst-pc16 on 9/13/16.
 */
public class RecentStoryAdapter extends RecyclerView.Adapter<RecentStoryAdapter.MyViewHolder> {

    private List<RecentStory> recentStories;
    public int isClicked = 0;
    public int isClicked1 = 0;
    public Boolean statusLike = false, statusDislike = false;
    public Context mContext;
    private DatabaseReference myRef;
    private FirebaseDatabase mFirebaseDatabase;
    private String android_id;
    public String userId;
    private String storyCount, likeStatus;
    String idUser;
    SharedPreferences settings;
    public static final String PREFS_NAME = "login";
    IoUpdateCallback mListener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CircleImageView profilepic;
        public TextView title, year, genre, timer, dislike, like, share;
        public ImageView unlikethump, likethump, sharebtn;
        RelativeLayout layRel;

        public MyViewHolder(View view) {
            super(view);
            profilepic = (CircleImageView) view.findViewById(R.id.title);
            genre = (TextView) view.findViewById(R.id.genre);
            year = (TextView) view.findViewById(R.id.year);
            timer = (TextView) view.findViewById(R.id.timer);
            dislike = (TextView) view.findViewById(R.id.dislike);
            like = (TextView) view.findViewById(R.id.like);
            share = (TextView) view.findViewById(R.id.share);
            unlikethump = (ImageView) view.findViewById(R.id.unlikethump);
            likethump = (ImageView) view.findViewById(R.id.likethump);
            sharebtn = (ImageView) view.findViewById(R.id.sharebtn);
            layRel=(RelativeLayout)view.findViewById(R.id.layRel);
            mFirebaseDatabase = FirebaseDatabase.getInstance();
            myRef = mFirebaseDatabase.getReference("StoryLine");
            android_id = Settings.Secure.getString(mContext.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            //idUser = settings.getString("idfire", "");
            settings = mContext.getSharedPreferences(PREFS_NAME, 0);
        }
    }

    public RecentStoryAdapter(List<RecentStory> recentStories, Context mContext, String userId, String storyCount, String likeStatus,IoUpdateCallback callback) {
        this.recentStories = recentStories;
        this.mContext = mContext;
        this.userId = userId;
        this.storyCount = storyCount;
        this.likeStatus = likeStatus;
        this.mListener =callback;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recent_story_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final RecentStory recentStory = recentStories.get(position);
        holder.setIsRecyclable(false);
        holder.year.setText(recentStory.getTitle());
        holder.genre.setText(recentStory.getGenre());
        holder.timer.setText(recentStory.getYear());
        HashMap<String, Long> likeCount = RecentStoryFragment.likeCount;
        HashMap<String, Long> dislikeCount = RecentStoryFragment.dislikeCount;
        holder.like.setText(recentStory.getLike());
        holder.dislike.setText(recentStory.getDisLike());

        Log.e("dislikecount", recentStory.getDislikeCount() + "");
        Log.e("likecount", recentStory.getStoryCount() + "");
        ArrayList<String> like = RecentStoryFragment.LikeMe;
        ArrayList<String> dislike = RecentStoryFragment.disLikeMe;

        for (int i = 0; i < like.size(); i++) {
            if (recentStory.getStoryId().equals(like.get(i))) {
                //  Log.e("Status", "true");
                holder.likethump.setImageResource(R.drawable.thumbsup);
                statusLike = true;
                break;
            }
        }
        for (int i = 0; i < dislike.size(); i++) {
            // Log.e("Story", recentStory.getStoryId());
//            Log.e("Like Story Id", like.get(i));
            if (recentStory.getStoryId().equals(dislike.get(i))) {
                //Log.e("Status", "true");
                holder.unlikethump.setImageResource(R.drawable.thumbsdown);
                statusDislike = true;
                break;
            }
        }
        if (!recentStory.getImageUrl().contains("http")) {
            try {
                byte[] encodeByte = Base64.decode(recentStory.getImageUrl(), Base64.DEFAULT);
                Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
                holder.profilepic.setImageBitmap(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Picasso.with(mContext).load(recentStory.getImageUrl()).into(holder.profilepic);
        }
        holder.likethump.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int postion = holder.getAdapterPosition();
                final RecentStory story = recentStories.get(postion);
                Log.d("data", "position = " + holder.getAdapterPosition());

                final Map<String, Object> taskMap = new HashMap<String, Object>();
                taskMap.put("like", holder.like.getText());
                taskMap.put("disLike", "0");
                taskMap.put("androidId", android_id);
                taskMap.put("profilePic", story.getImageUrl());
                taskMap.put("storyId", story.getStoryId());
                taskMap.put("time", story.getYear());
                taskMap.put("title", story.getTitle());
                taskMap.put("userName", story.getGenre());
                taskMap.put("userId", userId);

                myRef.child("disLikes").child(story.getStoryId()).child(userId).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(Task<Void> task) {
                        if (task.isSuccessful()) {
                            myRef.child("disLikes").child(story.getStoryId()).addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final Map<String, Object> taskMap = new HashMap<String, Object>();
                                    taskMap.put("disLike", "" + dataSnapshot.getChildrenCount());
                                    myRef.child("Recent_Story").child(story.getStoryId()).updateChildren(taskMap);
                                    notifyDataSetChanged();
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });
                        }
                    }
                });
                myRef.child("Likes").child(story.getStoryId()).child(userId).updateChildren(taskMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(Task<Void> task) {
                        if (task.isSuccessful()) {
                            myRef.child("Likes").child(story.getStoryId()).addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final Map<String, Object> taskMap = new HashMap<String, Object>();
                                    taskMap.put("like", "" + dataSnapshot.getChildrenCount());
                                    myRef.child("Recent_Story").child(story.getStoryId()).updateChildren(taskMap);
                                    notifyDataSetChanged();
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });
                        }
                    }
                });
            }
        });
        holder.unlikethump.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int postion = holder.getAdapterPosition();
                final RecentStory story = recentStories.get(postion);
                Log.d("data", "position = " + holder.getAdapterPosition());
                final Map<String, Object> taskMap = new HashMap<String, Object>();
                taskMap.put("like", "0");
                taskMap.put("disLike", holder.dislike.getText());
                taskMap.put("androidId", android_id);
                taskMap.put("profilePic", story.getImageUrl());
                taskMap.put("storyId", story.getStoryId());
                taskMap.put("time", story.getYear());
                taskMap.put("title", story.getTitle());
                taskMap.put("userName", story.getGenre());
                taskMap.put("userId", userId);
                myRef.child("Likes").child(story.getStoryId()).child(userId).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(Task<Void> task) {
                        if (task.isSuccessful()) {
                            myRef.child("Likes").child(story.getStoryId()).addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final Map<String, Object> taskMap = new HashMap<String, Object>();
                                    taskMap.put("like", "" + dataSnapshot.getChildrenCount());
                                    Log.e("total_count", dataSnapshot.getChildrenCount() + "");
                                    myRef.child("Recent_Story").child(story.getStoryId()).updateChildren(taskMap);
                                    notifyDataSetChanged();
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });
                        }
                    }
                });
                myRef.child("disLikes").child(story.getStoryId()).child(userId).updateChildren(taskMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(Task<Void> task) {
                        if (task.isSuccessful()) {
                            myRef.child("disLikes").child(story.getStoryId()).addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final Map<String, Object> taskMap = new HashMap<String, Object>();
                                    taskMap.put("disLike", "" + dataSnapshot.getChildrenCount());
                                    Log.e("total_count", dataSnapshot.getChildrenCount() + "");
                                    myRef.child("Recent_Story").child(story.getStoryId()).updateChildren(taskMap);
                                    notifyDataSetChanged();
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });
                        }
                    }
                });
            }
        });
        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int postion = holder.getAdapterPosition();
                final RecentStory story = recentStories.get(postion);
                Log.e("clkk", "clkk " + story.getTitle() + story.getDescription());
                FacebookSdk.sdkInitialize(mContext);
                mListener.update(story.getTitle(), story.getDescription());
            }

        });

        holder.layRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int postion = holder.getAdapterPosition();
                //final RecentStory story = recentStories.get(postion).getStoryId();
                Intent i=new Intent(mContext, StoryDetailsActivity.class).putExtra("storyId",recentStories.get(position).getStoryId()).putExtra("title",recentStories.get(position).getTitle().toString());
                mContext.startActivity(i);

            }
        });
    }

    @Override
    public int getItemCount() {
        return recentStories.size();
    }

}