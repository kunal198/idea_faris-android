package com.brstdev.ideafaris.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.brstdev.ideafaris.R;
import com.brstdev.ideafaris.activity.TabIconText;
import com.brstdev.ideafaris.model.FriendList;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by brst-pc16 on 9/13/16.
 */
public class TwitterPopUpFriendListAdapter extends RecyclerView.Adapter<TwitterPopUpFriendListAdapter.MyViewHolder> {

    private List<FriendList> friendList;
    private Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView imageView;
        public TextView friendname, friendemail;
        public CheckBox chkBox;
        public MyViewHolder(View view) {
            super(view);
            imageView = (CircleImageView) view.findViewById(R.id.title);
            friendname = (TextView) view.findViewById(R.id.friendname);
            //friendemail = (TextView) view.findViewById(R.id.friendemail);
            chkBox = (CheckBox) view.findViewById(R.id.chkBox);
        }
    }

    public TwitterPopUpFriendListAdapter(List<FriendList> friendList, Context mContext) {
        this.friendList = friendList;
        this.mContext = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.friend_list_pop_up_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final FriendList friendLists = friendList.get(position);
        holder.setIsRecyclable(false);
        holder.friendname.setText(friendLists.getFreindname());
        Log.e("nameeee", friendLists.getFreindname());
        if(friendLists.getStatus())
        holder.chkBox.setChecked(true);
        else
            holder.chkBox.setChecked(false);
        holder.chkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                     @Override
                                                     public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                         if(isChecked) {
                                                             friendLists.setStatus(true);
                                                         }
                                                         else {
                                                             friendLists.setStatus(false);
                                                         }
                                                         notifyDataSetChanged();
                                                     }
                                                 }
        );

        if (!friendLists.getImageUrl().contains("http")) {
            try {
                byte[] encodeByte = Base64.decode(friendLists.getImageUrl(), Base64.DEFAULT);
                Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
                holder.imageView.setImageBitmap(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Picasso.with(mContext).load(friendLists.getImageUrl()).into(holder.imageView);
        }
        //holder.friendemail.setText(friendLists.getFreindemail());
        // holder.imageView.setImageResource(friendLists.getImageUrl();
    }

    @Override
    public int getItemCount() {

        return friendList.size();
    }

}

