package com.brstdev.ideafaris.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brstdev.ideafaris.LoginActivity;
import com.brstdev.ideafaris.R;
import com.brstdev.ideafaris.adapter.GlobalStoryAdapter;
import com.brstdev.ideafaris.adapter.GlobalWriteAdapter;
import com.brstdev.ideafaris.fragment.GlobalStroyFragment;
import com.brstdev.ideafaris.model.GlobalStory;
import com.brstdev.ideafaris.model.Like;
import com.brstdev.ideafaris.model.WriteStoryFirebase;
import com.brstdev.ideafaris.utils.DividerItemDecoration;
import com.brstdev.ideafaris.utils.Utils;
import com.facebook.login.LoginManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.twitter.sdk.android.Twitter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

/**
 * Created by brst-pc16 on 9/13/16.
 */
public class WriteStoryActivity extends Activity {
    EditText writest;
    ImageView clock;
    Toolbar mToolbar;
    int time = 20;
    Timer t;
    TextView timer, savestory;
    TimerTask task;
    AlertDialog.Builder builder1;
    Dialog dialogTopic;
    private FirebaseDatabase mFirebaseDatabase;
    CountDownTimer mCountDownTimer;
    public static final String PREFS_NAME = "login";
    SharedPreferences settings;
    String idfire, name, logged, loggedtwitter, title, id;
    Boolean firstUser = true;
    int StoriesLeft = 99;
    //int StoriesLeft = 4;
    Dialog dialogFirst;
    int storyCount = 0, totalCount;
    Boolean popClick = false, onPause = false;
    SimpleDateFormat simpleDateFormat;
    String currentTime = "";
    Calendar calander;
    private DatabaseReference myRef, myRef1;
    ImageView imageView;
    long testTime = 0;
    String stTime = null;
    int randomNumber;
    String storyId;
    int count=0;
    private RecyclerView recyclerView;
    private String android_id;
    Boolean onCreate=true;
    Boolean addTitle=false;
    private GlobalWriteAdapter mAdapter;
    String text="Please write the first sentence of the Story";
    TextView txtNoStory;
    Boolean dialogclk=false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // mFirebaseDatabase = FirebaseDatabase.getInstance();
        // myRef1 = mFirebaseDatabase.getReference("StoryLine");
        //checkUser();
        setContentView(R.layout.write_story_activity);
        mAdapter = new GlobalWriteAdapter(GlobalStroyFragment.globalStoryList, this);
        recyclerView = (RecyclerView) findViewById(R.id.globalcontact);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);
        addTitle=false;
        testTime = Calendar.getInstance().getTimeInMillis();
        stTime = showMsgCounter(testTime);
        calander = Calendar.getInstance();
        simpleDateFormat = new SimpleDateFormat("MMMM d,yyyy hh:mm a");
        Log.e("testTime", "testTime " + testTime + "  " + stTime);
        android_id = Settings.Secure.getString(this.getContentResolver(),
                 Settings.Secure.ANDROID_ID);
        //Random r = new Random();
        // randomNumber = r.nextInt(80000 - 65000) + 6500;
        //randomNumber = 1205;
        storyId = getIntent().getExtras().getString("storyId").toString();
        Log.e("time", storyId + "");
        writest = (EditText) findViewById(R.id.writest);
        timer = (TextView) findViewById(R.id.timer);
        savestory = (TextView) findViewById(R.id.savestory);
        clock = (ImageView) findViewById(R.id.clock);
        txtNoStory=(TextView)findViewById(R.id.txtNoStory);
        if(GlobalStroyFragment.globalStoryList.size()==0)
            txtNoStory.setVisibility(View.VISIBLE);
        else
            txtNoStory.setVisibility(View.GONE);
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference("StoryLine");
        checkLoginUser();
        timer.setText("1m 00s");
        settings = getSharedPreferences(PREFS_NAME, 0);
        idfire = settings.getString("idfire", "");
        initToolbar();


        writest.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    // do your stuff here
                    Log.e("dfd", "dfdf");
                    showDialog();
                }
                return false;
            }
        });
        Log.e("editext", "" + timer.getText().toString());
        writest.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //if (writest.getText().toString().length() == 0 || timer.getText().toString().equalsIgnoreCase("done!"))
                // savestory.setEnabled(false);
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                /*if(s.toString().length()>5)
                {
                    Toast.makeText(WriteStoryActivity.this, "You cannot write story more than 149 words.", Toast.LENGTH_LONG).show();

                }*/
            }

            @Override
            public void afterTextChanged(Editable s) {
                savestory.setEnabled(true);
                savestory.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (firstUser)
                            addTopic();
                        else {
                            String story = writest.getText().toString();
                            // Toast.makeText(WriteStoryActivity.this, story, Toast.LENGTH_LONG).show();
                            mCountDownTimer.cancel();
                            timer.setText("done!");
                            clock.setVisibility(View.GONE);
                            //writest.setKeyListener(null);
                            savestory.setKeyListener(null);
                            idfire = settings.getString("idfire", "");
                            logged = settings.getString("logged", "");
                            loggedtwitter = settings.getString("loggedtwitter", "");
                            if (logged.equalsIgnoreCase("logged")) {
                                id = settings.getString("idtest", "");
                                name = settings.getString("name", "");
                            } else if (loggedtwitter.equalsIgnoreCase("loggedtwitter")) {
                                name = settings.getString("twittername", "");
                                id = settings.getString("twitterprofileimg", "");
                            }
                            calander = Calendar.getInstance();
                            simpleDateFormat = new SimpleDateFormat("MMMM d,yyyy hh:mm a");
                            currentTime = simpleDateFormat.format(calander.getTime());

                            Map<String, Object> taskMap = new HashMap<String, Object>();
                            taskMap.put("description", story);
                            taskMap.put("hasWriting", "false");
                            taskMap.put("title", "");
                            taskMap.put("topic", "");
                            taskMap.put("userName", settings.getString("name", ""));
                            taskMap.put("profilePic", settings.getString("idtest", ""));
                            taskMap.put("time", currentTime);
                            taskMap.put("userId", idfire);
                            taskMap.put("storyId",storyId);
                            //Toast.makeText(WriteStoryActivity.this, "call", Toast.LENGTH_SHORT).show();
                            myRef.child("Write").child(storyId).updateChildren(taskMap);
                            //  myRef.child("Write").push().setValue(new WriteStoryFirebase(story, "false", "", "", settings.getString("name", ""), settings.getString("idtest", ""), currentTime));
                            finish();
                        }
                    }


                });
            }
        });
    }
    /**
     * calculate the time
     *
     * @param time
     * @return
     */
    public static String showMsgCounter(long time) {
        Date startDate = new Date(time);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(startDate.getTime());

        Calendar now = Calendar.getInstance();
        long different = now.getTimeInMillis() - calendar.getTimeInMillis();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        // long daysInMilli = hoursInMilli * 24;

        // long elapsedDays = different / daysInMilli;
        // different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        // if (elapsedDays > 0) {
        // Calendar calendar2 = Calendar.getInstance();
        // calendar2.add(Calendar.DAY_OF_MONTH, -1);
        // if (calendar2.get(Calendar.DATE) == calendar.get(Calendar.DATE)) {
        // return "Yesterday";
        // }
        // return elapsedDays == 1 ? elapsedDays + " day ago" : elapsedDays + " days ago";

        if (elapsedHours > 0) {
            return elapsedHours == 1 ? elapsedHours + " hour ago" : elapsedHours > 24 ? "" : elapsedHours + " hours ago";
        } else if (elapsedMinutes > 0) {
            return elapsedMinutes == 1 ? elapsedMinutes + " minute ago" : elapsedMinutes + " minutes ago";
        }
        return "Just now";
    }

    void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
       /* setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayUseLogoEnabled(true);*/
        mToolbar.setNavigationIcon(R.drawable.arrow);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //finish();
                showDialog();
            }
        });
    }
    public void startTimer() {
        mCountDownTimer = new CountDownTimer(60000, 1000) { // adjust the milli seconds here
            //mCountDownTimer = new CountDownTimer(180000, 1000) { // adjust the milli seconds here
            public void onTick(long millisUntilFinished) {
                timer.setText("" + String.format("%dm:%02ds",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }
            public void onFinish() {
                timer.setText("done!");
                clock.setVisibility(View.GONE);
                //writest.setKeyListener(null);
                showDialog();
            }
        }.start();
    }
    void showDialog() {
        android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(this);
        builder1.setMessage("Perform action with Story.");
        builder1.setCancelable(true);
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Save",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int ida) {
                        dialog.cancel();
                        dialogclk=true;
                        // if (Integer.parseInt(savestory.getText().toString()) == 1) {
                        if(writest.getText().toString().equals(""))
                        {
                            Toast.makeText(WriteStoryActivity.this, "Please write your sentence.", Toast.LENGTH_SHORT).show();
                        }
                        else if (Integer.parseInt(savestory.getText().toString()) <=1) {
                            addTitle();
                            addTitle = true;
                            Log.e("count", savestory.getText().toString());
                        } else if (firstUser)
                            addTopic();
                        else {
                            String story = writest.getText().toString();
                            // Toast.makeText(WriteStoryActivity.this, story, Toast.LENGTH_LONG).show();
                            mCountDownTimer.cancel();
                            timer.setText("done!");
                            clock.setVisibility(View.GONE);
                            //writest.setKeyListener(null);
                            savestory.setKeyListener(null);
                            idfire = settings.getString("idfire", "");
                            logged = settings.getString("logged", "");
                            loggedtwitter = settings.getString("loggedtwitter", "");
                            if (logged.equalsIgnoreCase("logged")) {
                                id = settings.getString("idtest", "");
                                name = settings.getString("name", "");
                            } else if (loggedtwitter.equalsIgnoreCase("loggedtwitter")) {
                                name = settings.getString("twittername", "");
                                id = settings.getString("twitterprofileimg", "");
                            }
                             calander = Calendar.getInstance();
                             simpleDateFormat = new SimpleDateFormat("MMMM d,yyyy hh:mm a");
                             currentTime = simpleDateFormat.format(calander.getTime());
                            // String description, String hasWriting, String title,String topic,String userName,String profilePic,String time
                            Map<String, Object> taskMap = new HashMap<String, Object>();
                            taskMap.put("description", story);
                            taskMap.put("hasWriting", "false");
                            taskMap.put("title", "id");
                            taskMap.put("topic", "");
                            taskMap.put("userName", settings.getString("name", ""));
                            taskMap.put("profilePic", settings.getString("idtest", ""));
                            taskMap.put("time", currentTime);
                            taskMap.put("userId", idfire);
                             taskMap.put("storyId",storyId);
                            // Toast.makeText(WriteStoryActivity.this, "call", Toast.LENGTH_SHORT).show();
                            /*if(Integer.parseInt(savestory.getText().toString())==98) {
                                myRef.child("Write").child(idfire).push().updateChildren(taskMap);
                                Toast.makeText(WriteStoryActivity.this, "update", Toast.LENGTH_SHORT).show();
                            }*/
                            myRef.child("Write").child(storyId).updateChildren(taskMap);
                            //myRef.child("Write").push().setValue(new WriteStoryFirebase(story, "false", "id", "", settings.getString("name", ""), settings.getString("idtest", ""), currentTime));
                            settings.edit().putString("WriteStory", "true").commit();
                            Intent intMain = new Intent(WriteStoryActivity.this, TabIconText.class);
                            intMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intMain);
                            finish();
                        }
                    }
                });
        builder1.setNegativeButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        dialogclk=true;
                        calander = Calendar.getInstance();

                        simpleDateFormat = new SimpleDateFormat("MMMM d,yyyy hh:mm a");
                        currentTime = simpleDateFormat.format(calander.getTime());

                        Map<String, Object> taskMap = new HashMap<String, Object>();
                        taskMap.put("description", "");
                        taskMap.put("hasWriting", "false");
                        taskMap.put("title", "id");
                        taskMap.put("topic", "");
                        taskMap.put("userName", "");
                        taskMap.put("profilePic", "");
                        taskMap.put("time", currentTime);
                        // Toast.makeText(WriteStoryActivity.this, "call", Toast.LENGTH_SHORT).show();
                        taskMap.put("userId", idfire);
                        taskMap.put("storyId",storyId);
                        //myRef.child("Write").child(storyId).updateChildren(taskMap);
                        myRef.child("Write").child(storyId).removeValue();
                        // myRef.child("Write").push().setValue(new WriteStoryFirebase("", "false", "id", "", "", "",currentTime));
                        settings.edit().putString("WriteStory", "true").commit();
                        Intent intMain = new Intent(WriteStoryActivity.this, TabIconText.class);
                        intMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intMain);
                        finish();
                    }
                });
        android.app.AlertDialog alert11 = builder1.create();
        if (!onPause)
            alert11.show();
    }

    void checkLoginUser() {
        //myRef1.child("Write").removeValue();
        Log.e("checkuser", "userr");

        myRef.child("Write").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                storyCount = 0;
                // savestory.setText("99");
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    //Getting the data from snapshot
                    WriteStoryFirebase person = postSnapshot.getValue(WriteStoryFirebase.class);
                    Log.e("persn", "" + person.getDescription());
                    Log.e("persn", "" + person.getTopic());
                    Log.e("persn", "" + person.getTitle());
                    Log.e("persn", "" + person.getHasWriting());
                    if (person.getDescription()!=null&&!person.getDescription().equals("") && person.getHasWriting().equals("false") && !person.getTitle().equals("")) {
                        // Toast.makeText(WriteStoryActivity.this, "call", Toast.LENGTH_SHORT).show();
                        firstUser = false;
                        // storyCount=Integer.parseInt(snapshot.getChildrenCount() + "");
                        ++storyCount;
                        Log.e("meet", "mett");
                        //savestory.setText(String.valueOf(storyCount));
                    }

                    //Toast.makeText(WriteStoryActivity.this,"call",Toast.LENGTH_SHORT).show();
                    //Adding it to a string
                    String string = "Name: " + person.getDescription() + "\nAddress: " + person.getHasWriting() + "\n\n";
                    Log.e("kk", "string" + person.getHasWriting());
                    //Displaying it on textview
                }
                int count = 0;
                if (!popClick) {
                    count = StoriesLeft - storyCount;
                    savestory.setText(String.valueOf(count));
                    // if (count == 4) {
                    if (count == 99) {
                        if (onCreate) {
                            // Toast.makeText(WriteStoryActivity.this, "first", Toast.LENGTH_SHORT).show();
                            dialogFirst = new Dialog(WriteStoryActivity.this);
                            dialogFirst.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialogFirst.setContentView(R.layout.first_time_popup);
                            dialogFirst.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                            if (!dialogFirst.isShowing())
                                dialogFirst.show();
                            onCreate = false;
                            dialogFirst.setCancelable(false);
                            ImageView dailogButton = (ImageView) dialogFirst.findViewById(R.id.sweet);
                            dailogButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialogFirst.dismiss();
                                    calander = Calendar.getInstance();
                                    simpleDateFormat = new SimpleDateFormat("MMMM d,yyyy hh:mm a");
                                    currentTime = simpleDateFormat.format(calander.getTime());
                                    Log.e("save", "save");
                                    Map<String, Object> taskMap = new HashMap<String, Object>();
                                    taskMap.put("description", "");
                                    taskMap.put("hasWriting", "true");
                                    taskMap.put("title", "id");
                                    taskMap.put("topic", "");
                                    taskMap.put("userName", settings.getString("name", ""));
                                    taskMap.put("profilePic", settings.getString("idtest", ""));
                                    taskMap.put("time", currentTime);
                                    taskMap.put("userId", idfire);
                                    taskMap.put("storyId",storyId);
                                    // myRef.child("Write").child(storyId).updateChildren(taskMap);
                                    startTimer();
                                    popClick = true;
                                }
                            });
                        }
                    } else {
                        final Dialog dialog = new Dialog(WriteStoryActivity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.write_story_popup);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        dialog.show();
                        dialog.setCancelable(false);
                        ImageView dailogButton = (ImageView) dialog.findViewById(R.id.sweet);
                        imageView = (ImageView) dialog.findViewById(R.id.instruction);
                        dailogButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                calander = Calendar.getInstance();
                                simpleDateFormat = new SimpleDateFormat("MMMM d,yyyy hh:mm a");
                                currentTime = simpleDateFormat.format(calander.getTime());
                                Log.e("save", "save");
                                Map<String, Object> taskMap = new HashMap<String, Object>();
                                taskMap.put("description", "");
                                taskMap.put("hasWriting", "true");
                                taskMap.put("title", "");
                                taskMap.put("topic", "");
                                taskMap.put("userName", settings.getString("name", ""));
                                taskMap.put("profilePic", settings.getString("idtest", ""));
                                taskMap.put("time", currentTime);
                                taskMap.put("userId", idfire);
                                taskMap.put("storyId",storyId);
                                // myRef.child("Write").child(storyId).updateChildren(taskMap);
                                startTimer();
                                popClick = true;
                            }
                        });
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("sd", "sdf");
            }
        });
    }

    void addTopic() {
        dialogTopic = new Dialog(WriteStoryActivity.this);
        dialogTopic.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogTopic.setContentView(R.layout.topic_pop_up);
        dialogTopic.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogTopic.show();
        dialogTopic.setCancelable(false);
        ImageView myImageView = (ImageView) dialogTopic.findViewById(R.id.myImageView);
        final EditText editText = (EditText) dialogTopic.findViewById(R.id.myImageViewText);
        myImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(writest.getText().toString().equals(text))
                {
                    Toast.makeText(WriteStoryActivity.this, "Please add description of story", Toast.LENGTH_SHORT).show();
                }
                else if(editText.getText().toString().equals("")) {
                    Toast.makeText(WriteStoryActivity.this, "Please enter topic of story", Toast.LENGTH_SHORT).show();
                } else {
                    String story = writest.getText().toString();

                    // Toast.makeText(WriteStoryActivity.this, story, Toast.LENGTH_LONG).show();
                    mCountDownTimer.cancel();
                    timer.setText("done!");
                    clock.setVisibility(View.GONE);
                    //writest.setKeyListener(null);
                    savestory.setKeyListener(null);
                    idfire = settings.getString("idfire", "");
                    logged = settings.getString("logged", "");
                    loggedtwitter = settings.getString("loggedtwitter", "");
                    if (logged.equalsIgnoreCase("logged")) {
                        id = settings.getString("idtest", "");
                        name = settings.getString("name", "");
                    } else if (loggedtwitter.equalsIgnoreCase("loggedtwitter")) {
                        name = settings.getString("twittername", "");
                        id = settings.getString("twitterprofileimg", "");
                    }
                    calander = Calendar.getInstance();
                    simpleDateFormat = new SimpleDateFormat("MMMM d,yyyy hh:mm a");
                    currentTime = simpleDateFormat.format(calander.getTime());

                    Map<String, Object> taskMap = new HashMap<String, Object>();
                    taskMap.put("description", story);
                    taskMap.put("hasWriting", "false");
                    taskMap.put("title", "id");
                    taskMap.put("topic", editText.getText().toString());
                    taskMap.put("userName", settings.getString("name", ""));
                    taskMap.put("profilePic", settings.getString("idtest", ""));
                    taskMap.put("time", currentTime);
                    taskMap.put("userId", idfire);
                    taskMap.put("storyId",storyId);

                    myRef.child("Write").child(storyId).updateChildren(taskMap);
                    finish();
                    settings.edit().putString("WriteStory", "true").commit();
                    Intent intMain = new Intent(WriteStoryActivity.this, TabIconText.class);
                    intMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intMain);
                }
            }
        });

        if (!onPause)
            dialogTopic.show();
    }
    @Override
    protected void onPause() {
        super.onPause();
        onPause = true;
    }
    @Override
    public void onBackPressed() {
        showDialog();
    }
    void addTitle() {
        final Dialog dialog = new Dialog(WriteStoryActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.title_pop_up);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();
        ImageView myImageView = (ImageView) dialog.findViewById(R.id.myImageView);
        final EditText editTexts = (EditText) dialog.findViewById(R.id.myImageViewText);
        myImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (writest.getText().toString().equals(text)) {
                    Toast.makeText(WriteStoryActivity.this, "Please add description of story", Toast.LENGTH_SHORT).show();
                } else if (editTexts.getText().toString().equals("")) {
                    Toast.makeText(WriteStoryActivity.this, "Please enter title of story", Toast.LENGTH_SHORT).show();
                } else {
                    Utils.globalTitle="true";
                    String story = writest.getText().toString();
                    // Toast.makeText(WriteStoryActivity.this, story, Toast.LENGTH_LONG).show();
                    mCountDownTimer.cancel();
                    timer.setText("done!");
                    clock.setVisibility(View.GONE);
                    //writest.setKeyListener(null);
                    savestory.setKeyListener(null);
                    idfire = settings.getString("idfire", "");
                    logged = settings.getString("logged", "");
                    loggedtwitter = settings.getString("loggedtwitter", "");
                    if (logged.equalsIgnoreCase("logged")) {
                        id = settings.getString("idtest", "");
                        name = settings.getString("name", "");
                    } else if (loggedtwitter.equalsIgnoreCase("loggedtwitter")) {
                        name = settings.getString("twittername", "");
                        id = settings.getString("twitterprofileimg", "");
                    }
                    calander = Calendar.getInstance();
                    simpleDateFormat = new SimpleDateFormat("MMMM d,yyyy hh:mm a");
                    currentTime = simpleDateFormat.format(calander.getTime());
                    final Map<String, Object> taskMap = new HashMap<String, Object>();
                    taskMap.put("description", story);
                    taskMap.put("hasWriting", "false");
                    taskMap.put("title", editTexts.getText().toString());
                    taskMap.put("topic", "");
                    taskMap.put("userName", settings.getString("name", ""));
                    taskMap.put("profilePic", settings.getString("idtest", ""));
                    taskMap.put("time", currentTime);
                    taskMap.put("like", "0");
                    taskMap.put("disLike", "0");
                    taskMap.put("storyId", storyId);
                    taskMap.put("androidId", android_id);
                    taskMap.put("userId", idfire);

                    myRef.child("Write").child(storyId).updateChildren(taskMap);
                    myRef.child("Recent_Story").child(storyId).updateChildren(taskMap);
                    //myRef.child("All_Story").child(storyId).updateChildren(taskMap);
                    myRef.child("Write").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            ArrayList<HashMap<String, String>> userId = new ArrayList<HashMap<String, String>>();
                            Set<String> list = new HashSet<>();
                            for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                                HashMap<String, String> taskMapDetail = new HashMap<String, String>();
                                taskMapDetail.put(postSnapshot.child("userId").getValue().toString(), postSnapshot.child("userId").getValue().toString());
                                userId.add(taskMapDetail);
                                final Map<String, Object> taskMap1 = new HashMap<String, Object>();
                                taskMap1.put("userName", postSnapshot.child("userName").getValue().toString());
                                taskMap1.put("time", postSnapshot.child("time").getValue().toString());
                                taskMap1.put("description", postSnapshot.child("description").getValue().toString());
                                taskMap1.put("storyTime", postSnapshot.child("storyId").getValue().toString());
                                myRef.child("All_Story").child(storyId).child(idfire).push().setValue(taskMap1);
                                list.add(postSnapshot.child("userId").getValue().toString());
                            }
                            for (Iterator<String> it = list.iterator(); it.hasNext(); ) {
                                String f = it.next();
                                //Toast.makeText(WriteStoryActivity.this,f,Toast.LENGTH_SHORT).show();
                                Map<String, Object> taskMap = new HashMap<String, Object>();
                                taskMap.put("userId", idfire);
                                myRef.child("History").child(storyId).child(f).setValue(taskMap);
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                }
                myRef.child("Write").removeValue();
                // myRef.child("Write").push().setValue(new WriteStoryFirebase(story, "false", editTexts.getText().toString(), "", settings.getString("name", ""), settings.getString("idtest", ""), currentTime));
                finish();
                settings.edit().putString("WriteStory", "true").commit();
                Intent intMain = new Intent(WriteStoryActivity.this, TabIconText.class);
                intMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intMain);
            }
        });
    }

    void checkUser() {
        //myRef1.child("Write").removeValue();s
        Log.e("checkuser", "userr");
        myRef1.child("Write").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    //Getting the data from snapshot
                    WriteStoryFirebase person = postSnapshot.getValue(WriteStoryFirebase.class);
                    Log.e("persn", "" + person.getDescription());
                    Log.e("persn", "" + person.getTopic());
                    Log.e("persn", "" + person.getTitle());
                    Log.e("persn", "" + person.getHasWriting());
                    //Toast.makeText(WriteStoryActivity.this,"call",Toast.LENGTH_SHORT).show();
                    //Adding it to a string
                    String string = "Name: " + person.getDescription() + "\nAddress: " + person.getHasWriting() + "\n\n";
                    Log.e("detailks", "string" + person.getHasWriting());
                    Toast.makeText(WriteStoryActivity.this, "" + count, Toast.LENGTH_SHORT).show();

                    // Toast.makeText(getActivity(),person.getHasWriting()+"",Toast.LENGTH_SHORT).show();
                    if (person.getHasWriting().equals("true")) {
                        //writingStatus = true;
                        ++count;
                        Log.e("dsss", count + "ff");
                        // Toast.makeText(WriteStoryActivity.this,"twooo"+count,Toast.LENGTH_SHORT).show();
                        break;
                    }
                    if (count >= 1)
                        Toast.makeText(WriteStoryActivity.this, "finish", Toast.LENGTH_SHORT).show();
                    //Displaying it on textview
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("sd", "sdf");
            }
        });
    }
    @Override
    protected void onStop() {
        super.onStop();
        Log.e("kill","kill");
        if(!dialogclk) {
            calander = Calendar.getInstance();
            simpleDateFormat = new SimpleDateFormat("MMMM d,yyyy hh:mm a");
            currentTime = simpleDateFormat.format(calander.getTime());
            Map<String, Object> taskMap = new HashMap<String, Object>();
            taskMap.put("description", "");
            taskMap.put("hasWriting", "false");
            taskMap.put("title", "id");
            taskMap.put("topic", "");
            taskMap.put("userName", "");
            taskMap.put("profilePic", "");
            taskMap.put("time", currentTime);
            // Toast.makeText(WriteStoryActivity.this, "call", Toast.LENGTH_SHORT).show();
            taskMap.put("userId", idfire);
            taskMap.put("storyId",storyId);
            myRef.child("Write").child(storyId).removeValue();
            //myRef.child("Write").child(storyId).updateChildren(taskMap);
            // myRef.child("Write").push().setValue(new WriteStoryFirebase("", "false", "id", "", "", "",currentTime));
            settings.edit().putString("WriteStory", "true").commit();
            // Intent intMain = new Intent(WriteStoryActivity.this, TabIconText.class);
            // intMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            // startActivity(intMain);
            // finish();
            android.os.Process.killProcess(android.os.Process.myPid());
        }
    }
}

