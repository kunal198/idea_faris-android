package com.brstdev.ideafaris.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.brstdev.ideafaris.R;
import com.brstdev.ideafaris.adapter.FriendListAdapter;
import com.brstdev.ideafaris.adapter.TwitterPopUpFriendListAdapter;
import com.brstdev.ideafaris.model.FriendList;
import com.brstdev.ideafaris.utils.DividerItemDecoration;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import com.google.gson.Gson;
import com.twitter.sdk.android.Twitter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import twitter4j.DirectMessage;
import twitter4j.TwitterFactory;

/**
 * Created by brst-pc16 on 9/13/16.
 */
public class FriendListActivity extends AppCompatActivity {
    private List<FriendList> friendList = new ArrayList<>();
    private RecyclerView recyclerView;
    private FriendListAdapter mAdapter;
    Toolbar mToolbar;
    TextView profilename;
    CircleImageView image;
    TextView toolbar_title;
    public String logged;
    public String loggedtwitter;
    public static final String PREFS_NAME = "login";
    ProgressDialog dialog;
    SharedPreferences settings;
    Bitmap bitmap;
    Handler handler;
    String name, email, gender, id, location;
    ArrayList<String> twitterId = new ArrayList<>();
    Boolean fetchFriends = false;
    String nextFriends = "-1", nextFollowers = "-1";
    int preLast;
    RecyclerView recyclerview;
    private int previousTotal = 0;
    private boolean loading = true;
    private int visibleThreshold = 5;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    private int previousTotals = 0;
    private boolean loadings = true;
    private int visibleThresholds = 5;
    int firstVisibleItems, visibleItemCounts, totalItemCounts;
    Boolean chkBoxStatus=false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.freind_list_activity);
        profilename = (TextView) findViewById(R.id.profilename);
        recyclerView = (RecyclerView) findViewById(R.id.friendlist);
        image = (CircleImageView) findViewById(R.id.profilepic);
        //image.setImageResource(R.drawable.u4);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        initToolbar();
        settings = getSharedPreferences(PREFS_NAME, 0);
        logged = settings.getString("logged", "");
        loggedtwitter = settings.getString("loggedtwitter", "");

        Log.e("statuss", settings.getString("loginStausFacebook", ""));
        Log.e("fabb", settings.getString("clickFabIcon", ""));

        Button sendButton = (Button)findViewById(R.id.sendButton);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                new sendMessageTwitter().execute();
            }
        });
        mAdapter = new FriendListAdapter(friendList, this,chkBoxStatus);
        recyclerView.setAdapter(mAdapter);
        if (settings.getString("loginStausFacebook", "").equals("true") && settings.getString("clickFabIcon", "").equals("true")) {
            String appLinkUrl = "https://fb.me/887879311348903";
            String previewImageUrl = "https://firebasestorage.googleapis.com/v0/b/storyline-54d1f.appspot.com/o/logomain.png?alt=media&token=b1161dff-a739-4352-a1b0-b98c4b2cc9d8";
            if (AppInviteDialog.canShow()) {
                AppInviteContent content = new AppInviteContent.Builder()
                        .setApplinkUrl(appLinkUrl)
                        .setPreviewImageUrl(previewImageUrl)
                        .build();
                AppInviteDialog.show(this, content);
            }
        } else if (settings.getString("loginStausFacebook", "").equals("true") && settings.getString("clickFabIcon", "").equals("false")) {
            GraphRequestAsyncTask graphRequestAsyncTask = new GraphRequest(
                    //LoginResult.getAccessToken(),
                    AccessToken.getCurrentAccessToken(),
                    "/me/friends",
                    null,
                    HttpMethod.GET,
                    new GraphRequest.Callback() {
                        public void onCompleted(GraphResponse response) {
                            // Intent intent = new Intent(MainActivity.this,FriendsList.class);
                            try {
                                Log.e("raw name", response + "");
                                JSONArray rawName = response.getJSONObject().getJSONArray("data");
                                for (int i = 0; i < rawName.length(); i++) {
                                    JSONObject obj = rawName.getJSONObject(i);
                                    FriendList list = new FriendList();
                                    list.setFreindname(obj.getString("name"));
                                    list.setStatus(false);
                                    Log.e("namee", obj.getString("name"));
                                    list.setImageUrl("https://graph.facebook.com/" + obj.getString("id") + "/picture?width=80&height=80");
                                    friendList.add(list);
                                }
                                Collections.sort(friendList, new Comparator<FriendList>() {
                                    @Override
                                    public int compare(FriendList o1, FriendList o2) {
                                        return o1.getFreindname().compareToIgnoreCase(o2.getFreindname());
                                    }
                                });
//                                dialog.dismiss();
                                Log.e("sizesssk", friendList.size() + "");
                                mAdapter.notifyDataSetChanged();
                                Log.e("raw name", rawName + "");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ).executeAsync();
        }
           else if(settings.getString("loginStausFacebook", "").equals("false") )
            {
                new fetchTwitterFriends().execute();
            }
            // if (settings.getString("loginStausFacebook", "").equals("false")) {
           // }
            //new sendMessageTwitter().execute();
            final LinearLayoutManager mLayoutManager;
            mLayoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                /*LinearLayoutManager mLayoutManager;
                mLayoutManager = new LinearLayoutManager(FriendListActivity.this);
                recyclerView.setLayoutManager(mLayoutManager);
*/
                    visibleItemCount = recyclerView.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                    if (loading) {
                        if (totalItemCount > previousTotal) {
                            loading = false;
                            previousTotal = totalItemCount;
                        }
                    }
                    Log.e("nexttt", nextFriends + "");
                    if (!loading && (totalItemCount - visibleItemCount)
                            <= (firstVisibleItem + visibleThreshold)) {
                        // End has been reached
                       new fetchTwitterFriends().execute();
                        Log.e("Yaeye!", "end called");
                        loading = true;
                    }
                }
            });
            if (logged.equalsIgnoreCase("logged")) {
                name = settings.getString("name", "");
                profilename.setText(name);
                getprofilepicfb();
            } else if (loggedtwitter.equalsIgnoreCase("loggedtwitter")) {
                name = settings.getString("name", "");
                profilename.setText(name);
                getprofilepictwitter();
            }
        }
    void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar_title.setText("Friend List");
        mToolbar.setNavigationIcon(R.drawable.arrow);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    public void getprofilepicfb() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //Your code goes here
                    id = settings.getString("idtest", "");
                    if (id.contains("https://")) {
                        try {
                            URL url = new URL(id);
                            bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                            handler.sendEmptyMessage(0);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        byte[] encodeByte = Base64.decode(id, Base64.DEFAULT);
                        bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
                        handler.sendEmptyMessage(0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                image.setImageBitmap(bitmap);
                super.handleMessage(msg);
            }

        };
    }
    public void getprofilepictwitter() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //Your code goes here
                    id = settings.getString("idtest", "");
                    if (id.contains("http://")) {
                        try {
                            URL url = new URL(id);
                            bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                            handler.sendEmptyMessage(0);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        byte[] encodeByte = Base64.decode(id, Base64.DEFAULT);
                        bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
                        handler.sendEmptyMessage(0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                image.setImageBitmap(bitmap);
                super.handleMessage(msg);
            }

        };
    }

    public class fetchTwitterFriends extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(FriendListActivity.this);
            dialog.setMessage("Loading");
            dialog.setCancelable(false);
            dialog.show();
        }
        @Override
        protected Void doInBackground(String... params) {
            try {
                //WebServiceHandler web=new WebServiceHandler();
                Log.e("nextttfriendsss", "" + nextFriends);
                String response = getTwitterStream(settings.getString("screenName", ""), settings.getString("twitterId", ""));
                Log.e("responsee66", response);
                JSONObject obj = new JSONObject(response);
                //Log.e("yessStringg",obj.getString(""))
                nextFriends = String.valueOf(obj.getString("next_cursor_str"));
                // Log.e("nextInt",""+obj.getInt("next_cursor"));
                JSONArray array = obj.getJSONArray("users");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject twitterObject = array.getJSONObject(i);
                    FriendList list = new FriendList();
                    list.setFreindname(twitterObject.getString("name"));
                    list.setImageUrl(twitterObject.getString("profile_image_url"));
                    list.setId(twitterObject.getString("id_str"));
                    list.setStatus(false);
                    Log.e("nameee   ", twitterObject.getString("name") + " "+twitterObject.getString("id_str"));
                    Log.e("imggg", twitterObject.getString("id_str"));
                    friendList.add(list);
                }
                Log.e("lenthh", friendList.size() + "");
            } catch (Exception te) {
                te.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            //dialog.dismiss();
            //mAdapter.notifyDataSetChanged();
            new fetchTwitterFollowers().execute();
        }
    }

    public class fetchTwitterFollowers extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            try {
                //WebServiceHandler web=new WebServiceHandler();
                String response = getFollowers(settings.getString("screenName", ""), settings.getString("twitterId", ""));
                JSONObject obj1 = new JSONObject(response);
                nextFollowers = String.valueOf(obj1.getString("next_cursor_str"));
                JSONArray twtterarray = obj1.getJSONArray("users");
                for (int i = 0; i < twtterarray.length(); i++) {
                    JSONObject twitterObject = twtterarray.getJSONObject(i);
                    FriendList list = new FriendList();
                    list.setFreindname(twitterObject.getString("name"));
                    list.setImageUrl(twitterObject.getString("profile_image_url"));
                    list.setId(twitterObject.getString("id_str"));
                    Log.e("nameee11   ", twitterObject.getString("name") + " " + twitterObject.getString("id_str"));
                    list.setStatus(false);
                    friendList.add(list);
                }
                Log.e("sizeee", friendList.size() + "");
                Log.e("lenthh", twitterId.size() + "");
            } catch (Exception te) {
                te.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            dialog.dismiss();
            Collections.sort(friendList, new Comparator<FriendList>() {
                @Override
                public int compare(FriendList o1, FriendList o2) {
                    return o1.getFreindname().compareToIgnoreCase(o2.getFreindname());
                }
            });
            mAdapter.notifyDataSetChanged();
        }
    }
    private String getResponseBody(HttpRequestBase request) {
        StringBuilder sb = new StringBuilder();
        try {
            DefaultHttpClient httpClient = new DefaultHttpClient(new BasicHttpParams());
            HttpResponse response = httpClient.execute(request);
            int statusCode = response.getStatusLine().getStatusCode();
            String reason = response.getStatusLine().getReasonPhrase();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream inputStream = entity.getContent();

                BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
                String line = null;
                while ((line = bReader.readLine()) != null) {
                    sb.append(line);
                }
            } else {
                sb.append(reason);
            }
        } catch (UnsupportedEncodingException ex) {
        } catch (ClientProtocolException ex1) {
        } catch (IOException ex2) {
        }
        return sb.toString();
    }
    public String getTwitterStream(String screenName, String userId) {
        String results = null;
        // Step 1: Encode consumer key and secret
        try {
            // URL encode the consumer key and secret
            String urlApiKey = URLEncoder.encode("zJMhiUaLf6H7RNrFX1UaqEHdA", "UTF-8");
            String urlApiSecret = URLEncoder.encode("9Scf01y0U6QlSDsBSEzSuEGTzj6Bf6GXy2Hh640D2nIGsoBusI", "UTF-8");
            // Concatenate the encoded consumer key, a colon character, and the
            // encoded consumer secret
            String combined = urlApiKey + ":" + urlApiSecret;
            // Base64 encode the string

            String base64Encoded = Base64.encodeToString(combined.getBytes(), Base64.NO_WRAP);
            // Step 2: Obtain a bearer token

            HttpPost httpPost = new HttpPost("https://api.twitter.com/oauth2/token");
            httpPost.setHeader("Authorization", "Basic " + base64Encoded);
            httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
            httpPost.setEntity(new StringEntity("grant_type=client_credentials"));
            String rawAuthorization = getResponseBody(httpPost);
            Authenticated auth = jsonToAuthenticated(rawAuthorization);
            Log.e("resul", auth.token_type + "");
            // Applications should verify that the value associated with the
            // token_type key of the returned object is bearer
            if (auth != null && auth.token_type.equals("bearer")) {
                // Step 3: Authenticate API requests with bearer token
                //HttpGet httpGet = new HttpGet("https://api.twitter.com/1.1/friends/ids.json?stringify_ids=true&cursor=-1&screen_name=Brst Dev&user_id=109491200");

                HttpGet httpGet = new HttpGet("https://api.twitter.com/1.1/friends/list.json?stringify_ids=true&cursor=" + nextFriends + "&screen_name=" + screenName + "&user_id=" + userId);
                // header with the value of Bearer <>

                Log.e("urlllll", "https://api.twitter.com/1.1/friends/list.json?stringify_ids=true&cursor=" + nextFriends + "&screen_name=" + screenName + "&user_id=" + userId);
                httpGet.setHeader("Authorization", "Bearer " + auth.access_token);
                httpGet.setHeader("Content-Type", "application/json");
                // update the results with the body of the response
                results = getResponseBody(httpGet);
            }
        } catch (UnsupportedEncodingException ex) {
        } catch (IllegalStateException ex1) {
        }
        return results;
    }

    public String getFollowers(String screenName, String userId) {
        String results = null;
        // Step 1: Encode consumer key and secret
        try {
            // URL encode the consumer key and secret
            String urlApiKey = URLEncoder.encode("zJMhiUaLf6H7RNrFX1UaqEHdA", "UTF-8");
            String urlApiSecret = URLEncoder.encode("9Scf01y0U6QlSDsBSEzSuEGTzj6Bf6GXy2Hh640D2nIGsoBusI", "UTF-8");
            // Concatenate the encoded consumer key, a colon character, and the
            // encoded consumer secret
            String combined = urlApiKey + ":" + urlApiSecret;
            // Base64 encode the string

            String base64Encoded = Base64.encodeToString(combined.getBytes(), Base64.NO_WRAP);
            // Step 2: Obtain a bearer token

            HttpPost httpPost = new HttpPost("https://api.twitter.com/oauth2/token");
            httpPost.setHeader("Authorization", "Basic " + base64Encoded);
            httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
            httpPost.setEntity(new StringEntity("grant_type=client_credentials"));
            String rawAuthorization = getResponseBody(httpPost);
            Authenticated auth = jsonToAuthenticated(rawAuthorization);
            Log.e("resul", auth.token_type + "");
            // Applications should verify that the value associated with the
            // token_type key of the returned object is bearer
            if (auth != null && auth.token_type.equals("bearer")) {
                // Step 3: Authenticate API requests with bearer token
                //HttpGet httpGet = new HttpGet("https://api.twitter.com/1.1/friends/ids.json?stringify_ids=true&cursor=-1&screen_name=Brst Dev&user_id=109491200");
                HttpGet httpGet;
                httpGet = new HttpGet("https://api.twitter.com/1.1/followers/list.json?stringify_ids=true&cursor=" + nextFollowers + "&screen_name=" + screenName + "&user_id=" + userId);
                // header with the value of Bearer <>
                httpGet.setHeader("Authorization", "Bearer " + auth.access_token);
                httpGet.setHeader("Content-Type", "application/json");
                // update the results with the body of the response
                results = getResponseBody(httpGet);
            }
        } catch (UnsupportedEncodingException ex) {
        } catch (IllegalStateException ex1) {
        }
        return results;
    }

    // convert a JSON authentication object into an Authenticated object
    private Authenticated jsonToAuthenticated(String rawAuthorization) {
        Authenticated auth = null;
        if (rawAuthorization != null && rawAuthorization.length() > 0) {
            try {
                Gson gson = new Gson();
                auth = gson.fromJson(rawAuthorization, Authenticated.class);
            } catch (IllegalStateException ex) {
                // just eat the exception
            }
        }
        return auth;
    }

    public class Authenticated {
        String token_type;
        String access_token;
    }

    void showDialog() {


        AlertDialog.Builder alertDialog = new AlertDialog.Builder(FriendListActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View convertView = inflater.inflate(R.layout.twitter_list_pop_activity, null);
        alertDialog.setView(convertView);
        alertDialog.setTitle("Choose Friends");
        Log.e("list", "" + friendList.size());
        recyclerview = (RecyclerView) convertView.findViewById(R.id.friendTwitter);
        Button sendButton = (Button) convertView.findViewById(R.id.sendButton);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                new sendMessageTwitter().execute();
            }
        });

        //final LinearLayoutManager mLayout = (LinearLayoutManager) recyclerview
        //.getLayoutManager();
        //final LinearLayoutManager mLayout;
        // mLayout = new LinearLayoutManager(this);
        //recyclerview.setLayoutManager(mLayout);


        final LinearLayoutManager mLayout;
        mLayout = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayout);
        recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                Log.e("screooo", "scroll");
                /*LinearLayoutManager mLayoutManager;
                mLayoutManager = new LinearLayoutManager(FriendListActivity.this);
                recyclerView.setLayoutManager(mLayoutManager);
*/
                visibleItemCounts = recyclerview.getChildCount();
                totalItemCounts = mLayout.getItemCount();
                firstVisibleItems = mLayout.findFirstVisibleItemPosition();
                if (loadings) {
                    if (totalItemCounts > previousTotals) {
                        loadings = false;
                        previousTotals = totalItemCounts;
                    }
                }
                if (!loadings && (totalItemCounts - visibleItemCounts)
                        <= (firstVisibleItems + visibleThresholds)) {
                    // End has been reached
                    new fetchTwitterFriends().execute();
                    Log.e("Yaeye!", "end called");
                    loadings = true;
                }
            }
        });
        TwitterPopUpFriendListAdapter adapter = new TwitterPopUpFriendListAdapter(friendList, this);
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        recyclerview.setHasFixedSize(true);
        recyclerview.setAdapter(adapter);
        alertDialog.show();

    }

    public String sendMesages(String screenName, String userId) {
        String results = null;
        // Step 1: Encode consumer key and secret
        try {


            // URL encode the consumer key and secret
            String urlApiKey = URLEncoder.encode("zJMhiUaLf6H7RNrFX1UaqEHdA", "UTF-8");
            String urlApiSecret = URLEncoder.encode("9Scf01y0U6QlSDsBSEzSuEGTzj6Bf6GXy2Hh640D2nIGsoBusI", "UTF-8");
            // Concatenate the encoded consumer key, a colon character, and the
            // encoded consumer secret
            String combined = urlApiKey + ":" + urlApiSecret;
            // Base64 encode the string
            String base64Encoded = Base64.encodeToString(combined.getBytes(), Base64.NO_WRAP);
            // Step 2: Obtain a bearer token
            HttpPost httpPost = new HttpPost("https://api.twitter.com/oauth2/token");
            httpPost.setHeader("Authorization", "Basic " + base64Encoded);
            httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
            httpPost.setEntity(new StringEntity("grant_type=client_credentials"));
            String rawAuthorization = getResponseBody(httpPost);
            Authenticated auth = jsonToAuthenticated(rawAuthorization);
            Log.e("resul1111", auth.token_type + "");
            // Applications should verify that the value associated with the
            // token_type key of the returned object is bearer
            if (auth != null && auth.token_type.equals("bearer")) {
                // Step 3: Authenticate API requests with bearer token
                //HttpGet httpGet = new HttpGet("https://api.twitter.com/1.1/friends/ids.json?stringify_ids=true&cursor=-1&screen_name=Brst Dev&user_id=109491200");
                // https://api.twitter.com/1.1/direct_messages/new.json?text=hello%2C%20tworld.%20welcome%20to%201.1.&screen_name=theseancook
                /*HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPosts = new HttpPost("https://api.twitter.com/1.1/direct_messages/new.json?text=hello&screen_name=mahajankaran&user_id=109491200");
                try {

                    httpPosts.setHeader("Authorization", "Bearer " + auth.access_token);
                    httpPosts.setHeader("Content-Type", "application/json");
                    HttpResponse response = httpClient.execute(httpPost);
                    Log.e("yesss",getResponseBody(httpPosts));
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
                HttpPost htpPost;
                htpPost = new HttpPost("https://api.twitter.com/1.1/direct_messages/new.json?text=hello&screen_name=mahajankaran&user_id=109491200");
                // header with the value of Bearer <>
                htpPost.setHeader("Authorization", "Bearer " + auth.access_token);
                htpPost.setHeader("Content-Type", "application/json");
                // update the results with the body of the response
                results = getResponseBody(htpPost);
            }
        } catch (UnsupportedEncodingException ex) {
            Log.e("isser1", ex.toString());
        } catch (IllegalStateException ex1) {
            Log.e("issss", ex1.toString());
        }
        return results;
    }
    public class sendMessageTwitter extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(FriendListActivity.this);
            dialog.setMessage("Loading");
            dialog.setCancelable(false);
            dialog.show();
        }
        @Override
        protected Void doInBackground(String... params) {
            try {
                //WebServiceHandler web=new WebServiceHandler();
                Log.e("nextttfriendsss", "" + nextFriends);
                String response = sendMesages("", "");
                Log.e("finalsss", "jkk" + response);
                /*for(int i=0;i<friendList.size();i++)
                {
                    if(friendList.get(i).getStatus())
                    {
                        Log.e("idssssfinalss","jj"+friendList.get(i).getId());
                        //Log.e("finalsskkkkk","kkkkk");
                       // String response=sendMesages("", "");
                      //  Log.e("finalsskkkkk","kkkkk");
                       // Log.e("finalsss","jkk"+response);
                    }
                }*/
            } catch (Exception te) {
                te.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            dialog.dismiss();

        }
    }
}

