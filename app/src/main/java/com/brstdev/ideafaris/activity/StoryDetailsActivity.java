package com.brstdev.ideafaris.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.brstdev.ideafaris.R;
import com.brstdev.ideafaris.adapter.GlobalWriteAdapter;
import com.brstdev.ideafaris.model.GlobalStory;
import com.brstdev.ideafaris.utils.DividerItemDecoration;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class StoryDetailsActivity extends AppCompatActivity {
    private DatabaseReference myRef;
    private FirebaseDatabase mFirebaseDatabase;
    private GlobalWriteAdapter mAdapter;
    String storyId,title;
    Toolbar mToolbar;
    TextView txtTitle;
    private RecyclerView recyclerView;
    List<GlobalStory> globalStoryList = new ArrayList<>();
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_details);
        txtTitle=(TextView)findViewById(R.id.txtTitle);
        Log.e("storyid", getIntent().getExtras().get("storyId").toString());
        storyId=getIntent().getExtras().get("storyId").toString();
        title=getIntent().getExtras().get("title").toString();
        txtTitle.setText("Title: " + title);
        Log.e("title", title);
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference("StoryLine");
        dialog=new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.show();
        mAdapter = new GlobalWriteAdapter(globalStoryList, this);
        recyclerView = (RecyclerView) findViewById(R.id.globalcontact);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        prepareList();
        initToolbar();
    }
    private void prepareList() {
        //pd.show();
        Log.e("checkuser", "userr");
        myRef.child("All_Story").child(storyId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    //Getting the data from snapshot
                    Iterator i = postSnapshot.getChildren().iterator();
                    while (i.hasNext()) {
                        DataSnapshot s = (DataSnapshot) i.next();
                        String userId = s.child("description").getValue().toString();
                        Log.e("yesman", userId);
                        GlobalStory story=new GlobalStory();
                        story.setGenre(s.child("userName").getValue().toString());
                        story.setYear(s.child("description").getValue().toString());
                        story.setStoryTime(s.child("storyTime").getValue().toString());
                        try {
                            SimpleDateFormat format = new SimpleDateFormat("MMMM d,yyyy hh:mm a");
                            Calendar calander = Calendar.getInstance();
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM d,yyyy hh:mm a");
                            String currentTime = simpleDateFormat.format(calander.getTime());
                            Date Date1 = format.parse(currentTime);
                            Date Date2 = format.parse(s.child("time").getValue().toString());
                            long mills = Date1.getTime() - Date2.getTime();
                            Log.v("Data1", "" + Date1.getTime());
                            Log.v("Data2", "" + Date2.getTime());
                            int Hours = (int) (mills / (1000 * 60 * 60));
                            int Mins = (int) (mills / (1000 * 60)) % 60;
                            String diff = Hours + ":" + Mins; // updated value every1 second
                            Log.e("detailss", diff);
                            if (Mins == 0 && Hours < 1) {
                                story.setTime("Just Now");
                            } else if (Mins <= 59 && Hours < 1) {
                                story.setTime(String.valueOf(Mins) + " Minutes");
                            } else if (Hours >= 1 && Hours <= 24) {
                                story.setTime(String.valueOf(Hours) + " Hours");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        globalStoryList.add(story);
                    }
                    dialog.dismiss();
                    Collections.sort(globalStoryList, new Comparator<GlobalStory>() {
                        @Override
                        public int compare(GlobalStory o1, GlobalStory o2) {
                            return Long.compare(Long.parseLong(o1.getStoryTime()),
                                    Long.parseLong(o2.getStoryTime()));
                        }
                    });
                    Collections.reverse(globalStoryList);
                    mAdapter.notifyDataSetChanged();

                    //Log.e("dislike", disLike.get(count));
                    //RecentStory movie = new RecentStory(person.getTitle(), person.getUserName(), person.getTime(), person.getProfilePic(), person.getAndroidId(), format(Long.parseLong(person.getLike())), format(Long.parseLong(person.getDisLike())), person.getStoryId(), storyCount.get(count), like.get(count), disLikeCount.get(count), disLike.get(count), person.getDescription());
                    // recentStories.add(movie);
                    // ++count;
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        mToolbar.setNavigationIcon(R.drawable.arrow);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("desttt", "desdd");
    }
}
