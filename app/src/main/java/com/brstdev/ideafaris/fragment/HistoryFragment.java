package com.brstdev.ideafaris.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.brstdev.ideafaris.MainAcivity;
import com.brstdev.ideafaris.R;
import com.brstdev.ideafaris.activity.TabIconText;
import com.brstdev.ideafaris.adapter.HistoryAdapter;
import com.brstdev.ideafaris.model.History;
import com.brstdev.ideafaris.model.Like;
import com.brstdev.ideafaris.model.RecentStory;
import com.brstdev.ideafaris.model.TopRated;
import com.brstdev.ideafaris.utils.DividerItemDecoration;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

/**
 * Created by brst-pc16 on 9/14/16.
 */
public class HistoryFragment extends Fragment {
    private List<History> histories = new ArrayList<>();
    private List<History> filterHistories = new ArrayList<>();


    private RecyclerView recyclerView;
    private HistoryAdapter mAdapter;
    View rootView;
    Toolbar mToolbar;
    TextView toolbar_title;
    private DatabaseReference myRef1;
    private FirebaseDatabase mFirebaseDatabase;
    String idUser;
    SharedPreferences settings;
    public static final String PREFS_NAME = "login";
    TextView noStoryYet;
    ArrayList<String> userStoryId;
    Boolean status = true;

    private static final NavigableMap<Long, String> suffixes = new TreeMap<>();
    static {
        suffixes.put(1_000L, "k");
        suffixes.put(1_000_000L, "M");
        suffixes.put(1_000_000_000L, "G");
        suffixes.put(1_000_000_000_000L, "T");
        suffixes.put(1_000_000_000_000_000L, "P");
        suffixes.put(1_000_000_000_000_000_000L, "E");
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.history_activity, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.historylist);
        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        toolbar_title = (TextView) rootView.findViewById(R.id.toolbar_title);
        noStoryYet = (TextView) rootView.findViewById(R.id.noStoryYet);
        mAdapter = new HistoryAdapter(filterHistories, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
        idUser = settings.getString("idfire", "");
        myRef1 = mFirebaseDatabase.getReference("StoryLine");
        userStoryId = new ArrayList<>();
        myRef1.child("History").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //recentStories.clear();
                //pd.dismiss();
                userStoryId = new ArrayList<String>();
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    //Getting the data from snapshot
                    Log.e("getkeyy", postSnapshot.getKey());
                    Log.e("userId", idUser);
                    String storyId = postSnapshot.getKey();
                    Iterator i = postSnapshot.getChildren().iterator();
                    while (i.hasNext()) {
                        DataSnapshot s = (DataSnapshot) i.next();
                        if (s.getKey().toString().equals(idUser)) {
                            userStoryId.add(storyId);
                        }
                    }
                }
                if (userStoryId.size()==0) {
                    noStoryYet.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                } else {
                    noStoryYet.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
                    for (int i = 0; i < userStoryId.size(); i++) {
                        Log.e("belongssto", userStoryId.get(i));
                        myRef1.child("Recent_Story")
                                .addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot snapshot) {
                                        Log.e("valuess", "" + snapshot + "");
                                        histories.clear();
                                        filterHistories.clear();
                                        String time="";

                                        //Toast.makeText(getActivity(),dataSnapshot.getChildrenCount()+"",Toast.LENGTH_SHORT).show();
                                        for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                                            //Getting the data from snapshot
                                            Log.e("romy", "" + snapshot.child("userId").getValue());
                                            try {
                                                SimpleDateFormat format = new SimpleDateFormat("MMMM d,yyyy hh:mm a");
                                                Calendar calander = Calendar.getInstance();
                                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM d,yyyy hh:mm a");
                                                String currentTime = simpleDateFormat.format(calander.getTime());
                                                Date Date1 = format.parse(currentTime);
                                                Date Date2 = format.parse(postSnapshot.child("time").getValue().toString());
                                                long mills = Date1.getTime() - Date2.getTime();
                                                Log.v("Data1", "" + postSnapshot.child("time").getValue().toString());
                                                Log.v("Data2", "" + Date2.getTime());
                                                int Hours = (int) (mills / (1000 * 60 * 60));
                                                int Mins = (int) (mills / (1000 * 60)) % 60;
                                                String diff = Hours + ":" + Mins; // updated value every1 second
                                                Log.e("detailss", diff);
                                                if (Mins == 0 && Hours < 1) {
                                                    time = "Just Now";
                                                    //person.setTime("Just Now");
                                                } else if (Mins <= 59 && Hours < 1) {
                                                    time = String.valueOf(Mins) + " Minutes";
                                                    //person.setTime(String.valueOf(Mins) + " Minutes");
                                                } else if (Hours >= 1 && Hours <= 24) {
                                                    //person.setTime(String.valueOf(Hours) + " Hours");
                                                    time = String.valueOf(Hours) + " Hours";
                                                }
                                                else
                                                {
                                                    time=postSnapshot.child("time").getValue().toString();
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            History top = new History(postSnapshot.child("profilePic").getValue().toString(), postSnapshot.child("userName").getValue().toString(), postSnapshot.child("title").getValue().toString(), format(Long.parseLong(postSnapshot.child("like").getValue().toString())), postSnapshot.child("storyId").getValue().toString(), time, format(Long.parseLong(postSnapshot.child("like").getValue().toString())), format(Long.parseLong(postSnapshot.child("disLike").getValue().toString())));
                                            histories.add(top);
                                        }
                                        for (int i = 0; i < userStoryId.size(); i++) {
                                            String storyId = userStoryId.get(i);
                                            for (int j = 0; j < histories.size(); j++) {
                                                if (storyId.equals(histories.get(j).getStoryId())) {
                                                    History top = new History(histories.get(j).getImageUrl(), histories.get(j).getGenre(), histories.get(j).getYear(), histories.get(j).getTitle(), histories.get(j).getStoryId(), histories.get(j).getTime(), histories.get(j).getLike(), histories.get(j).getDisLike());
                                                    filterHistories.add(top);
                                                    status = false;

                                                }
                                            }
                                        }
                                        mAdapter.notifyDataSetChanged();
                                        Collections.reverse(filterHistories);
                                        Log.e("finalsssslenth", filterHistories.size() + "");
                                    }
                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                    }
                                });
                    }
                    //Log.e("sizessssssss", userStoryId.size() + "");
                }

                @Override
                public void onCancelled (DatabaseError databaseError){
                }

            }

            );

            // Toast.makeText(getActivity(), userStoryId.size() + "  " + histories.size() +" "+filterHistories.size() , Toast.LENGTH_SHORT).show();

            /*if(status)
            {
                noStoryYet.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                status=false;
            }
            else
            {
                noStoryYet.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }*/
       /* myRef1.child("History").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    if(idUser.equals(postSnapshot.child("userId").getValue().toString()))
                    Log.e("historyy", postSnapshot.child("storyId").getValue().toString());
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });*/
            initToolbar();
            // prepareMovieData();
            return rootView;
        }

    void initToolbar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayUseLogoEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar_title.setText("History");

        mToolbar.setNavigationIcon(R.drawable.arrow);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainAcivity.class);
                startActivity(intent);
            }
        });
    }
    public static String format(long value) {
        //Long.MIN_VALUE == -Long.MIN_VALUE so we need an adjustment here
        if (value == Long.MIN_VALUE) return format(Long.MIN_VALUE + 1);
        if (value < 0) return "-" + format(-value);
        if (value < 1000) return Long.toString(value); //deal with easy case

        Map.Entry<Long, String> e = suffixes.floorEntry(value);
        Long divideBy = e.getKey();
        String suffix = e.getValue();

        long truncated = value / (divideBy / 10); //the number part of the output times 10
        boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
        return hasDecimal ? (truncated / 10d) + suffix : (truncated / 10) + suffix;
    }
}
