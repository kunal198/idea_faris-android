package com.brstdev.ideafaris.fragment;

import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.brstdev.ideafaris.MainAcivity;
import com.brstdev.ideafaris.R;
import com.brstdev.ideafaris.activity.TabIconText;
import com.brstdev.ideafaris.adapter.TopRatedAdapter;
import com.brstdev.ideafaris.model.Like;
import com.brstdev.ideafaris.model.PrivateStory;
import com.brstdev.ideafaris.model.RecentStory;
import com.brstdev.ideafaris.model.TopRated;
import com.brstdev.ideafaris.model.WriteStoryFirebase;
import com.brstdev.ideafaris.utils.DividerItemDecoration;
import com.brstdev.ideafaris.utils.IoUpdateCallback;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.ShareOpenGraphObject;
import com.facebook.share.widget.ShareDialog;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

import io.fabric.sdk.android.Fabric;

/**
 * Created by brst-pc16 on 9/14/16.
 */
public class TopRatedFragment extends Fragment implements IoUpdateCallback {
    View rootView;
    private List<TopRated> globalStoryList = new ArrayList<>();
    private RecyclerView recyclerView;
    TopRatedAdapter mAdapter;
    Toolbar mToolbar;
    TextView toolbar_title,txtNoStoryYet;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef1;
    static CallbackManager callbackManager;
    static ShareDialog shareDialog;
    SharedPreferences settings;
    public static final String PREFS_NAME = "login";
    private static final NavigableMap<Long, String> suffixes = new TreeMap<>();
    static {
        suffixes.put(1_000L, "k");
        suffixes.put(1_000_000L, "M");
        suffixes.put(1_000_000_000L, "G");
        suffixes.put(1_000_000_000_000L, "T");
        suffixes.put(1_000_000_000_000_000L, "P");
        suffixes.put(1_000_000_000_000_000_000L, "E");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);
        rootView = inflater.inflate(R.layout.top_rated_activity, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.toprated);
        txtNoStoryYet = (TextView) rootView.findViewById(R.id.txtNoStory);
        toolbar_title=(TextView)rootView.findViewById(R.id.toolbar_title);
        mAdapter = new TopRatedAdapter(globalStoryList,getActivity(),this);
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef1 = mFirebaseDatabase.getReference("StoryLine");
        settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
        FacebookSdk.sdkInitialize(getActivity());
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        Query myTopPostsQuery = myRef1.child("Recent_Story")
                .orderByChild("like");
        Log.e("detailss_finalss",""+myTopPostsQuery);

        fetchTopRated();

        mToolbar=(Toolbar)rootView.findViewById(R.id.toolbar);
        initToolbar();
        // prepareMovieData();
        return rootView;
    }
    void initToolbar(){
        ShareOpenGraphObject object = new ShareOpenGraphObject.Builder()
                .putString("og:type", "books.book")
                .putString("og:title", "A Game of Thrones")
                .putString("og:description", "In the frozen wastes to the north of Winterfell, sinister and supernatural forces are mustering.")
                .putString("books:isbn", "0-553-57340-3")
                .build();
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()). getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((AppCompatActivity) getActivity()). getSupportActionBar().setHomeButtonEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayUseLogoEnabled(true);
        ((AppCompatActivity) getActivity()). getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar_title.setText("Top Rated");
        mToolbar.setNavigationIcon(R.drawable.arrow);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainAcivity.class);
                startActivity(intent);
            }
        });
    }
    void fetchTopRated()
    {
        //Query myTopPostsQuery = myRef1.child("Recent_Story")
                //.orderByChild("like");
        myRef1.child("Recent_Story").orderByChild("like").limitToFirst(10)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        Log.e("valuess", "" + snapshot + "");
                        globalStoryList.clear();
                        //Toast.makeText(getActivity(),dataSnapshot.getChildrenCount()+"",Toast.LENGTH_SHORT).show();
                        for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                            //Getting the data from snapshot
                            Log.e("Sharan", "" + postSnapshot.child("like").getValue());
                            if (!postSnapshot.child("like").getValue().toString().equals("0")) {
                                TopRated top = new TopRated(postSnapshot.child("profilePic").getValue().toString(), postSnapshot.child("userName").getValue().toString(), postSnapshot.child("title").getValue().toString(), format(Long.parseLong(postSnapshot.child("like").getValue().toString())), postSnapshot.child("storyId").getValue().toString(), postSnapshot.child("description").getValue().toString());
                                globalStoryList.add(top);
                            }
                        }
                        Collections.reverse(globalStoryList);
                        mAdapter.notifyDataSetChanged();
                        if (globalStoryList.size() == 0) {
                            recyclerView.setVisibility(View.GONE);
                            txtNoStoryYet.setVisibility(View.VISIBLE);
                        } else {
                            recyclerView.setVisibility(View.VISIBLE);
                            txtNoStoryYet.setVisibility(View.GONE);
                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }
    @Override
    public void update(String title, String description) {

        Uri uri=null;
        try {
            uri  = Uri.parse("www.Storyline.com" + "://" + getResources().getResourcePackageName(R.mipmap.logomain) + '/' + getResources().getResourceTypeName(R.mipmap.logomain) + '/' + String.valueOf(R.mipmap.logomain));
            //uri = Uri.parse("android.resource://com.brstdev.ideafaris/drawable/c4.png");
            InputStream stream = getActivity().getContentResolver().openInputStream(uri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (settings.getString("loginStausFacebook", "").equals("true")) {
            if (ShareDialog.canShow(ShareLinkContent.class)) {
                ShareLinkContent linkContent = new ShareLinkContent.Builder()
                        .setContentTitle(title)
                        .setContentDescription(
                                description)
                        .setContentUrl(uri)
                        .build();
                shareDialog.show(linkContent);
            } }else {
                if (appInstalledOrNot("com.twitter.android")) {
                    TwitterAuthConfig authConfig = new TwitterAuthConfig("zJMhiUaLf6H7RNrFX1UaqEHdA", "9Scf01y0U6QlSDsBSEzSuEGTzj6Bf6GXy2Hh640D2nIGsoBusI");
                    Fabric.with(getActivity(), new TwitterCore(authConfig), new TweetComposer());
                    TweetComposer.Builder builder = new TweetComposer.Builder(getActivity())
                            .text(title + "\n" + description)
                    .image(uri);
                    builder.show();
                } else {
                    Toast.makeText(getActivity(), "Please install twitter app first", Toast.LENGTH_SHORT).show();
                }
        }
    }
    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getActivity().getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }
    private void sendEmail() {

        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "subject");
        intent.putExtra(android.content.Intent.EXTRA_TEXT, "https://developers.facebook.com/");
        intent.putExtra(Intent.EXTRA_TITLE, "title");
        startActivity(Intent.createChooser(intent, "dialogHeaderText"));
    }

    public static String format(long value) {
        //Long.MIN_VALUE == -Long.MIN_VALUE so we need an adjustment here
        if (value == Long.MIN_VALUE) return format(Long.MIN_VALUE + 1);
        if (value < 0) return "-" + format(-value);
        if (value < 1000) return Long.toString(value); //deal with easy case

        Map.Entry<Long, String> e = suffixes.floorEntry(value);
        Long divideBy = e.getKey();
        String suffix = e.getValue();

        long truncated = value / (divideBy / 10); //the number part of the output times 10
        boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
        return hasDecimal ? (truncated / 10d) + suffix : (truncated / 10) + suffix;
    }

}





