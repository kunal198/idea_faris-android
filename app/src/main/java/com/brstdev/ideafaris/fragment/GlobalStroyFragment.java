package com.brstdev.ideafaris.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.brstdev.ideafaris.MainAcivity;
import com.brstdev.ideafaris.R;

import com.brstdev.ideafaris.activity.TabIconText;
import com.brstdev.ideafaris.activity.WriteStoryActivity;
import com.brstdev.ideafaris.adapter.GlobalStoryAdapter;
import com.brstdev.ideafaris.model.GlobalStory;
import com.brstdev.ideafaris.model.WriteStoryFirebase;
import com.brstdev.ideafaris.utils.DividerItemDecoration;
import com.brstdev.ideafaris.utils.Utils;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by brst-pc16 on 9/14/16.
 */
public class GlobalStroyFragment extends Fragment {
    public static List<GlobalStory> globalStoryList = new ArrayList<>();
    private RecyclerView recyclerView;
    private GlobalStoryAdapter mAdapter;
    Toolbar mToolbar;
    Button writestory;
    TextView toolbar_title;
    private FirebaseAuth mAuth;

    private FirebaseDatabase mFirebaseDatabase;
    private FirebaseUser mFireBaseUser;
    private DatabaseReference myRef;
    private DatabaseReference myRef1;
    ProgressDialog pd;
    String writeName, writeTitle;
    String name, email, gender, id, location, idfire;
    public static final String PREFS_NAME = "login";
    SharedPreferences settings;
    public String logged;
    public String loggedtwitter;
    Boolean writingStatus = false;
    String topic = "gggggggggggggggggggggggggggggggggggggggggggggggg";
    TextView txtNoStory;
    Boolean internet = false,click=false,loop=false;
    InterstitialAd mInterstitialAd;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.global_story, container,
                false);
        Utils.globalTitle="false";
        Utils.openActivity="false";

        if (!internetCheck()) {
            initToolbar(rootView);
            Toast.makeText(getActivity(), "Please connect to working internet", Toast.LENGTH_SHORT).show();
        } else
            initConditions(rootView);

        // Toast.makeText(getActivity(),"ss661",Toast.LENGTH_SHORT).show();

        return rootView;
    }

    void initToolbar(View rootView) {
        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        toolbar_title = (TextView) rootView.findViewById(R.id.toolbar_title);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayUseLogoEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar_title.setText("Current Story");
        mToolbar.setNavigationIcon(R.drawable.arrow);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainAcivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
    }

    public void readfire() {
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the UI
                GlobalStory post = dataSnapshot.getValue(GlobalStory.class);
                Log.d("post", post.toString());
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("", "loadPost:onCancelled", databaseError.toException());
                // [START_EXCLUDE]
               /* Toast.makeText(getActivity(), "Failed to load post.",
                        Toast.LENGTH_SHORT).show();*/
                // [END_EXCLUDE]
            }
        };

    }

    private void prepareMovieData() {
        readfire();

    }

    public void readData() {
        myRef1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                WriteStoryFirebase value = dataSnapshot.getValue(WriteStoryFirebase.class);
                Log.d("", "Value is: " + value);

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("", "Failed to read value.", error.toException());
            }
        });
        Query query = myRef1.orderByKey().equalTo(idfire);
        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.e("Data", dataSnapshot.getValue().toString());
                Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
                // writeName=value.get("name").toString();
                // writeTitle=value.get("title").toString();

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    void checkLoginUser() {
        //myRef1.child("Write").removeValue();
        myRef1.child("Write").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    //Getting the data from snapshot
                    WriteStoryFirebase person = postSnapshot.getValue(WriteStoryFirebase.class);
                    if (person.getHasWriting().equals("true")) {
                        writingStatus = true;
                    }

                    //Adding it to a string
                    String string = "Name: " + person.getDescription() + "\nAddress: " + person.getHasWriting() + "\n\n";
                    Log.e("finalss", "string" + person.getHasWriting());

                    //Displaying it on textview
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    void checkUser() {
        myRef1.child("Write").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    //Getting the data from snapshot
                    WriteStoryFirebase person = postSnapshot.getValue(WriteStoryFirebase.class);
                    if (person.getHasWriting().equals("true")) {
                        writingStatus = true;
                         break;
                    }
            }}
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    void checkTopics() {
        //myRef1.child("Write").removeValue();s
        Log.e("checkuser", "userr");
        myRef1.child("Write").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                globalStoryList.clear();
                pd.dismiss();
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    //Getting the data from snapshot
                    WriteStoryFirebase person = postSnapshot.getValue(WriteStoryFirebase.class);
                    Log.e("persn", "" + person.getDescription());
                    Log.e("persn", "" + person.getTopic());
                    Log.e("persn", "" + person.getTitle());
                    Log.e("persn", "" + person.getHasWriting());
                    try {
                        SimpleDateFormat format = new SimpleDateFormat("MMMM d,yyyy hh:mm a");
                        Calendar calander = Calendar.getInstance();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM d,yyyy hh:mm a");
                        String currentTime = simpleDateFormat.format(calander.getTime());
                        Date Date1 = format.parse(currentTime);
                        Date Date2 = format.parse(person.getTime());
                        long mills = Date1.getTime() - Date2.getTime();
                        Log.v("Data1", "" + Date1.getTime());
                        Log.v("Data2", "" + Date2.getTime());
                        int Hours = (int) (mills / (1000 * 60 * 60));
                        int Mins = (int) (mills / (1000 * 60)) % 60;
                        String diff = Hours + ":" + Mins; // updated value every1 second
                        Log.e("detailss", diff);
                        if (Mins == 0 && Hours < 1) {
                            person.setTime("Just Now");
                        } else if (Mins <= 59 && Hours < 1) {
                            person.setTime(String.valueOf(Mins) + " Minutes");
                        } else if (Hours >= 1 && Hours <= 24) {
                            person.setTime(String.valueOf(Hours) + " Hours");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    writingStatus = false;
                    // Toast.makeText(getActivity(),"this false",Toast.LENGTH_SHORT).show();
                    if (person.getDescription()!=null&&!person.getDescription().equals("") && !person.getProfilePic().equals("") && !person.getUserName().equals("")) {
                        GlobalStory movie = new GlobalStory(person.getUserName(), person.getDescription()
                                , person.getProfilePic().toString(), person.getTime().toString(), "loc", "phone", "", ""
                        );
                        globalStoryList.add(movie);
                    }
                }
                Collections.reverse(globalStoryList);
                mAdapter.notifyDataSetChanged();
                if (globalStoryList.size() == 0) {
                    txtNoStory.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                } else {
                    txtNoStory.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    private boolean internetCheck() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    void initConditions(View rootView) {
        pd = new ProgressDialog(getActivity());
        pd.setMessage("loading");
        pd.show();
        pd.setCanceledOnTouchOutside(false);
        settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
        if(settings.getInt("count",0)%3==0)
        {

            mInterstitialAd = new InterstitialAd(getActivity());
            mInterstitialAd.setAdUnitId("ca-app-pub-2808887337728226/3772309398");
            AdRequest adRequest = new AdRequest.Builder()
                    .build();
            // Load ads into Interstitial Ads
            mInterstitialAd.loadAd(adRequest);
            mInterstitialAd.setAdListener(new AdListener() {
                public void onAdLoaded() {
                    showInterstitial();
                }
            });
        }
        logged = settings.getString("logged", "");
        loggedtwitter = settings.getString("loggedtwitter", "");
        idfire = settings.getString("idfire", "");

        if (logged.equalsIgnoreCase("logged")) {
            name = settings.getString("name", "");
        } else if (loggedtwitter.equalsIgnoreCase("loggedtwitter")) {
            name = settings.getString("twittername", "");
        }
        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.globalcontact);
        writestory = (Button) rootView.findViewById(R.id.writestory);
        txtNoStory = (TextView) rootView.findViewById(R.id.txtNoStory);
        mAdapter = new GlobalStoryAdapter(globalStoryList, getActivity().getApplicationContext());
        //mAdapter=new GlobalStoryAdapter(globalStoryList);
        toolbar_title = (TextView) rootView.findViewById(R.id.toolbar_title);
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference("StoryLine");
        myRef1 = mFirebaseDatabase.getReference("StoryLine");
        checkTopics();
       // readData();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);
        initToolbar(rootView);
       // prepareMovieData();
       // checkLoginUser();
        checkUser();
        writestory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkUser();
                if (writingStatus) {
                    Toast.makeText(getActivity(), "Someone is writing the Story", Toast.LENGTH_LONG).show();
                } else {
                    String android_id = Settings.Secure.getString(getActivity().getContentResolver(),
                            Settings.Secure.ANDROID_ID);
                    Long tsLong = System.currentTimeMillis();
                    String ts = tsLong.toString();
                    Map<String, Object> taskMap = new HashMap<String, Object>();
                    taskMap.put("description", "");
                    taskMap.put("hasWriting", "true");
                    taskMap.put("title", "");
                    taskMap.put("topic", "");
                    taskMap.put("userName", settings.getString("+name", ""));
                    taskMap.put("profilePic", settings.getString("idtest", ""));
                    taskMap.put("time", "");
                    taskMap.put("androidId", android_id);
                    taskMap.put("userId", idfire);
                    //ts = "Story_" + ts;
                     myRef.child("Write").child(ts).setValue(taskMap);
                    Log.e("time", ts + "");
                    //Toast.makeText(getActivity(),"open Activity "+loop,Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(), WriteStoryActivity.class).putExtra("storyId", ts);
                    startActivity(intent);

                }}
        });
    }
    private void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }
}
