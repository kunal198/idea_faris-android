package com.brstdev.ideafaris.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brstdev.ideafaris.MainAcivity;
import com.brstdev.ideafaris.R;
import com.brstdev.ideafaris.activity.FriendListActivity;
import com.brstdev.ideafaris.activity.PrivateWriteStoryActivity;
import com.brstdev.ideafaris.activity.WriteStoryActivity;
import com.brstdev.ideafaris.adapter.GlobalStoryAdapter;
import com.brstdev.ideafaris.adapter.PrivateStoryAdapter;
import com.brstdev.ideafaris.model.FriendList;
import com.brstdev.ideafaris.model.GlobalStory;
import com.brstdev.ideafaris.model.PrivateStory;
import com.brstdev.ideafaris.model.WriteStoryFirebase;
import com.brstdev.ideafaris.utils.DividerItemDecoration;
import com.brstdev.ideafaris.utils.Utils;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.AppInviteDialog;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by brst-pc16 on 9/14/16.
 */
public class GlobalStoryFragmentPrivate extends Fragment {

    public static List<GlobalStory> globalStoryList = new ArrayList<>();
    private RecyclerView recyclerView;
    private GlobalStoryAdapter mAdapter;
    Toolbar mToolbar;
    Button writestory;
    TextView toolbar_title;
    private FirebaseAuth mAuth;
    ArrayList<FriendList> friendList=new ArrayList();
    String nextFriends = "-1", nextFollowers = "-1";
    private FirebaseDatabase mFirebaseDatabase;
    private FirebaseUser mFireBaseUser;
    private DatabaseReference myRef;
    private DatabaseReference myRef1;
    FloatingActionButton fab;
    private CoordinatorLayout layoutCordinate;
    ProgressDialog pd;
    String writeName, writeTitle;
    String name, email, gender, id, location, idfire;
    public static final String PREFS_NAME = "login";
    SharedPreferences settings;
    SharedPreferences.Editor edit;
    public String logged;
    public String loggedtwitter;
    String writingStatus = "false";
    String topic = "ggg";
    TextView txtNoStory;
    ProgressDialog dialog;
    Boolean status=false;
    InterstitialAd mInterstitialAd;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.private_story, container,
                false);
        pd = new ProgressDialog(getActivity());
        pd.setMessage("loading...");
        pd.setCanceledOnTouchOutside(false);
        Utils.openActivity="false";
        layoutCordinate=(CoordinatorLayout)rootView.findViewById(R.id.cordinate);
        settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
        edit=settings.edit();
        if(settings.getInt("count",0)%3==0)
        {
            mInterstitialAd = new InterstitialAd(getActivity());
            mInterstitialAd.setAdUnitId("ca-app-pub-2808887337728226/3772309398");
            AdRequest adRequest = new AdRequest.Builder()
                    .build();
            // Load ads into Interstitial Ads
            mInterstitialAd.loadAd(adRequest);
            mInterstitialAd.setAdListener(new AdListener() {
                public void onAdLoaded() {
                    showInterstitial();
                }
            });
        }
        logged = settings.getString("logged", "");
        loggedtwitter = settings.getString("loggedtwitter", "");
        idfire = settings.getString("idfire", "");
        if (logged.equalsIgnoreCase("logged")) {
            name = settings.getString("name", "");
        } else if (loggedtwitter.equalsIgnoreCase("loggedtwitter")) {
            name = settings.getString("twittername", "");
        }
        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.privatecontact);
        writestory = (Button) rootView.findViewById(R.id.writestory);
        txtNoStory = (TextView) rootView.findViewById(R.id.txtNoStory);
        mAdapter = new GlobalStoryAdapter(globalStoryList, getActivity().getApplicationContext());
        //mAdapter=new GlobalStoryAdapter(globalStoryList);
        toolbar_title = (TextView) rootView.findViewById(R.id.toolbar_title);
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference("StoryLine").child("Users");
        myRef1 = mFirebaseDatabase.getReference("StoryLine");
        fab=(FloatingActionButton)rootView.findViewById(R.id.fab);
        if (settings.getString("loginStausFacebook", "").equals("true")) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    pd.show();
                    fetchUserFriends();
                }
            });
        }
        else
        {
            new fetchTwitterFriends().execute();
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(settings.getString("loginStausFacebook", "").equals("true"))
                {
                    edit.putString("clickFabIcon","true").commit();
                }
                else if(settings.getString("loginStausFacebook","").equals("false"))
                {
                    edit.putString("clickFabIcon","true").commit();
                }
                Log.e("clickkkksssss","clkk");
                if (settings.getString("loginStausFacebook", "").equals("true") && settings.getString("clickFabIcon", "").equals("true")) {
                    String appLinkUrl = "https://fb.me/887879311348903";
                    String previewImageUrl = "https://firebasestorage.googleapis.com/v0/b/storyline-54d1f.appspot.com/o/logomain.png?alt=media&token=b1161dff-a739-4352-a1b0-b98c4b2cc9d8";
                    if (AppInviteDialog.canShow()) {
                        AppInviteContent content = new AppInviteContent.Builder()
                                .setApplinkUrl(appLinkUrl)
                                .setPreviewImageUrl(previewImageUrl)
                                .build();
                        AppInviteDialog.show(getActivity(), content);
                    }
                }
                else {
                    Intent intent = new Intent(getActivity(), FriendListActivity.class);
                    startActivity(intent);
                }
            }
        });
        //readData();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);
        initToolbar();
        //prepareMovieData();
        //checkLoginUser();
        //checkLoginUser();
        writestory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // checkLoginUser();
               // new checkLoginUser().execute();
                Utils.status="false";
                if (writingStatus.equals("true")) {
                    Toast.makeText(getActivity(), "Someone writing a story", Toast.LENGTH_LONG).show();
                } else {
                    Calendar calander = Calendar.getInstance();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm");
                    String time = simpleDateFormat.format(calander.getTime());
                    Log.e("timee", time);
                   // Toast.makeText(getActivity(),time,Toast.LENGTH_SHORT).show();
                    String android_id = Settings.Secure.getString(getActivity().getContentResolver(),
                            Settings.Secure.ANDROID_ID);
                    Long tsLong = System.currentTimeMillis();
                    String ts = tsLong.toString();
                    Map<String, Object> taskMap = new HashMap<String, Object>();
                    taskMap.put("description", "");
                    taskMap.put("hasWriting", "true");
                    taskMap.put("title", "");
                    taskMap.put("topic", "");
                    taskMap.put("userName", settings.getString("+name", ""));
                    taskMap.put("profilePic", settings.getString("idtest", ""));
                    taskMap.put("time", "");
                    taskMap.put("androidId", android_id);
                    taskMap.put("userId", idfire);
                    taskMap.put("storyTime",time);
                    taskMap.put("storyId", ts + "");
                    myRef1.child("Friends Only").child(settings.getString("userId", "")).child(tsLong+"").setValue(taskMap);

                    //ts = "Story_" + ts;
                    // myRef.child("Write").child(ts).setValue(taskMap);
                    Log.e("time", ts + "");
                    //Toast.makeText(getActivity(),"open Activity "+loop,Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(), PrivateWriteStoryActivity.class).putExtra("storyId", ts).putExtra("storyCount",globalStoryList.size()+"").putExtra("storyList",""+globalStoryList);startActivity(intent);
                }

               }
        });
        return rootView;
    }
    void initToolbar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayUseLogoEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar_title.setText("Current Story");
        mToolbar.setNavigationIcon(R.drawable.arrow);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainAcivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
    }

    /*public void readfire() {
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the UI
                GlobalStory post = dataSnapshot.getValue(GlobalStory.class);
                Log.d("post", post.toString());
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("", "loadPost:onCancelled", databaseError.toException());
                // [START_EXCLUDE]
               *//* Toast.makeText(getActivity(), "Failed to load post.",
                        Toast.LENGTH_SHORT).show();*//*
                // [END_EXCLUDE]
            }
        };
    }
    private void prepareMovieData() {
        readfire();
    }*/

    public void readData() {
        myRef1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                WriteStoryFirebase value = dataSnapshot.getValue(WriteStoryFirebase.class);
                Log.d("", "Value is: " + value);
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("", "Failed to read value.", error.toException());
            }
        });
        Query query = myRef1.orderByKey().equalTo(idfire);
        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Log.e("Data", dataSnapshot.getValue().toString());
                Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
                // writeName=value.get("name").toString();
                // writeTitle=value.get("title").toString();


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    void checkLoginUser() {
        {
            myRef1.child("Friends Only").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    //fetchUserFriends();
                    globalStoryList.clear();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        Iterator i = postSnapshot.getChildren().iterator();
                        Log.e("finalsskeyy", postSnapshot.getKey());
                        for (int j = 0; j < friendList.size(); j++) {
                            Log.e("frindssid", friendList.get(j).getId() + "");
                            if (friendList.get(j).getId().equals(postSnapshot.getKey())) {
                                String time = "";
                                while (i.hasNext()) {
                                    DataSnapshot s = (DataSnapshot) i.next();
                                    String userId = s.child("hasWriting").getValue().toString();
                                    Log.e("userIdssss", userId);
                                    if(s.child("hasWriting").getValue().toString().equals("true"))
                                    {
                                        writingStatus = s.child("hasWriting").getValue().toString();
                                        Toast.makeText(getActivity(),"true",Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        }
                    }
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });

            if (writingStatus.equals("true")) {
                Toast.makeText(getActivity(), "Someone is writing the Story", Toast.LENGTH_LONG).show();
            } else {

                String android_id = Settings.Secure.getString(getActivity().getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                Long tsLong = System.currentTimeMillis();
                String ts = tsLong.toString();
                Map<String, Object> taskMap = new HashMap<String, Object>();
                taskMap.put("description", "");
                taskMap.put("hasWriting", "true");
                taskMap.put("title", "");
                taskMap.put("topic", "");
                taskMap.put("userName", settings.getString("+name", ""));
                taskMap.put("profilePic", settings.getString("idtest", ""));
                taskMap.put("time", "");
                taskMap.put("androidId", android_id);
                taskMap.put("userId", idfire);
                taskMap.put("storyId",ts+"");
                myRef1.child("Friends Only").child(settings.getString("userId", "")).child(tsLong+"").setValue(taskMap);
                //ts = "Story_" + ts;
                // myRef.child("Write").child(ts).setValue(taskMap);
                Log.e("time", ts + "");
                //Toast.makeText(getActivity(),"open Activity "+loop,Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), PrivateWriteStoryActivity.class).putExtra("storyId", ts).putExtra("storyCount",globalStoryList.size()+"").putExtra("storyList",""+globalStoryList);
                startActivity(intent);
            }


        }
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //Toast.makeText(getActivity(),"ss11",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Toast.makeText(getActivity(),"ss66",Toast.LENGTH_SHORT).show();
    }

    void checkTopics() {
        myRef1.child("Friends Only").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //fetchUserFriends();
                globalStoryList.clear();
                status=false;
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    Iterator i = postSnapshot.getChildren().iterator();
                    Log.e("finalsskeyy", postSnapshot.getKey());
                    for (int j = 0; j < friendList.size(); j++) {
                        Log.e("frindssid", friendList.get(j).getId() + "");
                        if (friendList.get(j).getId().equals(postSnapshot.getKey())) {
                            String time = "";
                            while (i.hasNext()) {
                                DataSnapshot s = (DataSnapshot) i.next();
                                String userId = s.child("description").getValue().toString();
                                Log.e("userIdssss", userId);
                                try {
                                    SimpleDateFormat format = new SimpleDateFormat("MMMM d,yyyy hh:mm a");
                                    Calendar calander = Calendar.getInstance();
                                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM d,yyyy hh:mm a");
                                    String currentTime = simpleDateFormat.format(calander.getTime());
                                    Date Date1 = format.parse(currentTime);
                                    Date Date2 = format.parse(s.child("time").getValue().toString());
                                    long mills = Date1.getTime() - Date2.getTime();
                                    Log.v("Data1", "" + Date1.getTime());
                                    Log.v("Data2", "" + Date2.getTime());
                                    int Hours = (int) (mills / (1000 * 60 * 60));
                                    int Mins = (int) (mills / (1000 * 60)) % 60;
                                    String diff = Hours + ":" + Mins; // updated value every1 second
                                    Log.e("detailss", diff);
                                    if (Mins == 0 && Hours < 1) {
                                        time = "Just Now";
                                        // person.setTime("Just Now");
                                    } else if (Mins <= 59 && Hours < 1) {
                                        time = String.valueOf(Mins) + " Minutes";
                                        //person.setTime(String.valueOf(Mins) + " Minutes");
                                    } else if (Hours >= 1 && Hours <= 24) {
                                        time = String.valueOf(Hours) + " Hours";
                                        //person.setTime(String.valueOf(Hours) + " Hours");
                                    } else {
                                        time = s.child("time").getValue().toString();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                if(s.child("hasWriting").getValue().toString().equals("true"))
                                {
                                    writingStatus = s.child("hasWriting").getValue().toString();
                                    status=true;
                                }
                               // Log.e("haswritingg", s.child("hasWriting").getValue().toString());
                                if (!s.child("description").getValue().toString().equals("") && !s.child("profilePic").getValue().toString().equals("") && !s.child("userName").getValue().toString().equals("")) {
                                    GlobalStory movie = new GlobalStory(s.child("userName").getValue().toString(), s.child("description").getValue().toString()
                                            , s.child("profilePic").getValue().toString(), time, s.child("userId").getValue().toString(), s.child("storyId").getValue().toString(),s.child("storyTime").getValue().toString(),s.child("time").getValue().toString()
                                    );
                                    globalStoryList.add(movie);
                                }
                            }
                        }
                    }
                }
                globalStoryList.size();
                Collections.sort(globalStoryList, new Comparator<GlobalStory>() {
                    @Override
                    public int compare(GlobalStory o1, GlobalStory o2) {
                        return Long.compare(Long.parseLong(o1.getStoryId()),
                                Long.parseLong(o2.getStoryId()));
                    }
                });
                /*Collections.sort(globalStoryList, new Comparator<GlobalStory>() {
                    @Override
                    public int compare(GlobalStory o1, GlobalStory o2) {
                        try {
                            return new SimpleDateFormat("hh:mm").parse(o1.getStoryTime()).compareTo(new SimpleDateFormat("hh:mm").parse(o2.getStoryTime()));
                        } catch (ParseException e) {
                            return 0;
                        }
                    }
                });*/
                pd.dismiss();
                Collections.reverse(globalStoryList);
                mAdapter.notifyDataSetChanged();
                if (globalStoryList.size() == 0) {
                    txtNoStory.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    Utils.status="false";
                } else {
                    txtNoStory.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
                if(!status)
                    writingStatus="false";
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        /*if(globalStoryList.size()==3)
        {
            Utils.status="true";
        }
        else
        {
            Utils.status="false";
        }*/
       // Toast.makeText(getActivity(),""+globalStoryList.size(),Toast.LENGTH_SHORT).show();
    }

    void fetchUserFriends()
    {
        GraphRequestAsyncTask graphRequestAsyncTask = new GraphRequest(
                //LoginResult.getAccessToken(),
                AccessToken.getCurrentAccessToken(),
                "/me/friends",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        // Intent intent = new Intent(MainActivity.this,FriendsList.class);
                        try {
                            Log.e("raw name", response + "");
                            JSONArray rawName = response.getJSONObject().getJSONArray("data");
                            FriendList object=new FriendList();
                            object.setId(settings.getString("userId", ""));
                            friendList.add(object);
                            for(int i=0;i<rawName.length();i++)
                            {
                                JSONObject obj=rawName.getJSONObject(i);
                                FriendList list=new FriendList();
                                list.setFreindname(obj.getString("name"));
                                list.setId(obj.getString("id"));
                                Log.e("namee",obj.getString("name"));
                                list.setImageUrl("https://graph.facebook.com/" + obj.getString("id") + "/picture?width=80&height=80");
                                friendList.add(list);
                            }
                            checkTopics();
                            Log.e("raw name",rawName+"");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ).executeAsync();
    }
    public class fetchTwitterFriends extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Loading");
            dialog.setCancelable(false);
            dialog.show();
        }
        @Override
        protected Void doInBackground(String... params) {
            try {
                //WebServiceHandler web=new WebServiceHandler();
                String response = getTwitterStream(settings.getString("screenName", ""), settings.getString("twitterId", ""));
                Log.e("responsee66", response);
                JSONObject obj = new JSONObject(response);
                FriendList object=new FriendList();
                object.setId(settings.getString("userId", ""));
                friendList.add(object);
                //Log.e("yessStringg",obj.getString(""))
                // Log.e("nextInt",""+obj.getInt("next_cursor"));
                JSONArray array = obj.getJSONArray("ids");
                for (int i = 0; i < array.length(); i++) {
                    //JSONObject twitterObject = array.getJSONObject(i);
                    FriendList list = new FriendList();
                    //list.setFreindname(twitterObject.getString("name"));
                   // list.setImageUrl(twitterObject.getString("profile_image_url"));
                    list.setId(array.get(i).toString());
                   // list.setStatus(false);
                    friendList.add(list);
                }
                Log.e("lenthh", friendList.size() + "");
            } catch (Exception te) {
                te.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            //dialog.dismiss();
            //mAdapter.notifyDataSetChanged();
            new fetchTwitterFollowers().execute();
        }
    }

    public class fetchTwitterFollowers extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            try {
                //WebServiceHandler web=new WebServiceHandler();
                String response = getFollowers(settings.getString("screenName", ""), settings.getString("twitterId", ""));
                JSONObject obj1 = new JSONObject(response);
                JSONArray twtterarray = obj1.getJSONArray("ids");
                for (int i = 0; i < twtterarray.length(); i++) {
                   // JSONObject twitterObject = twtterarray.getJSONObject(i);
                    FriendList list = new FriendList();
                    //list.setFreindname(twitterObject.getString("name"));
                   // list.setImageUrl(twitterObject.getString("profile_image_url"));
                    list.setId(twtterarray.get(i).toString());
                   // Log.e("nameee11   ", twitterObject.getString("name") + " " + twitterObject.getString("id_str"));

                   // list.setStatus(false);
                    friendList.add(list);
                }
                Log.e("sizeee", friendList.size() + "");
            } catch (Exception te) {
                te.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            dialog.dismiss();
            checkTopics();
            //mAdapter.notifyDataSetChanged();
            /*if(settings.getString("loginStausFacebook", "").equals("false") && settings.getString("clickFabIcon", "").equals("true"))
            {
                 showDialog();
            }*/
        }
    }
    private String getResponseBody(HttpRequestBase request) {
        StringBuilder sb = new StringBuilder();
        try {
            DefaultHttpClient httpClient = new DefaultHttpClient(new BasicHttpParams());
            HttpResponse response = httpClient.execute(request);
            int statusCode = response.getStatusLine().getStatusCode();
            String reason = response.getStatusLine().getReasonPhrase();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream inputStream = entity.getContent();

                BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
                String line = null;
                while ((line = bReader.readLine()) != null) {
                    sb.append(line);
                }
            } else {
                sb.append(reason);
            }
        } catch (UnsupportedEncodingException ex) {
        } catch (ClientProtocolException ex1) {
        } catch (IOException ex2) {
        }
        return sb.toString();
    }
    public String getTwitterStream(String screenName, String userId) {
        String results = null;
        // Step 1: Encode consumer key and secret
        try {
            // URL encode the consumer key and secret
            String urlApiKey = URLEncoder.encode("zJMhiUaLf6H7RNrFX1UaqEHdA", "UTF-8");
            String urlApiSecret = URLEncoder.encode("9Scf01y0U6QlSDsBSEzSuEGTzj6Bf6GXy2Hh640D2nIGsoBusI", "UTF-8");
            // Concatenate the encoded consumer key, a colon character, and the
            // encoded consumer secret
            String combined = urlApiKey + ":" + urlApiSecret;
            // Base64 encode the string

            String base64Encoded = Base64.encodeToString(combined.getBytes(), Base64.NO_WRAP);
            // Step 2: Obtain a bearer token

            HttpPost httpPost = new HttpPost("https://api.twitter.com/oauth2/token");
            httpPost.setHeader("Authorization", "Basic " + base64Encoded);
            httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
            httpPost.setEntity(new StringEntity("grant_type=client_credentials"));
            String rawAuthorization = getResponseBody(httpPost);
            Authenticated auth = jsonToAuthenticated(rawAuthorization);
            Log.e("resul", auth.token_type + "");
            // Applications should verify that the value associated with the
            // token_type key of the returned object is bearer
            if (auth != null && auth.token_type.equals("bearer")) {
                // Step 3: Authenticate API requests with bearer token
                //HttpGet httpGet = new HttpGet("https://api.twitter.com/1.1/friends/ids.json?stringify_ids=true&cursor=-1&screen_name=Brst Dev&user_id=109491200");

                HttpGet httpGet = new HttpGet("https://api.twitter.com/1.1/friends/ids.json?stringify_ids=true&cursor=" + nextFriends + "&screen_name=" + screenName + "&user_id=" + userId);
                // header with the value of Bearer <>

                Log.e("urlllll", "https://api.twitter.com/1.1/friends/list.json?stringify_ids=true&cursor=" + nextFriends + "&screen_name=" + screenName + "&user_id=" + userId);
                httpGet.setHeader("Authorization", "Bearer " + auth.access_token);
                httpGet.setHeader("Content-Type", "application/json");
                // update the results with the body of the response
                results = getResponseBody(httpGet);
            }
        } catch (UnsupportedEncodingException ex) {
        } catch (IllegalStateException ex1) {
        }
        return results;
    }

    public String getFollowers(String screenName, String userId) {
        String results = null;
        // Step 1: Encode consumer key and secret
        try {
            // URL encode the consumer key and secret
            String urlApiKey = URLEncoder.encode("zJMhiUaLf6H7RNrFX1UaqEHdA", "UTF-8");
            String urlApiSecret = URLEncoder.encode("9Scf01y0U6QlSDsBSEzSuEGTzj6Bf6GXy2Hh640D2nIGsoBusI", "UTF-8");
            // Concatenate the encoded consumer key, a colon character, and the
            // encoded consumer secret
            String combined = urlApiKey + ":" + urlApiSecret;
            // Base64 encode the string

            String base64Encoded = Base64.encodeToString(combined.getBytes(), Base64.NO_WRAP);
            // Step 2: Obtain a bearer token

            HttpPost httpPost = new HttpPost("https://api.twitter.com/oauth2/token");
            httpPost.setHeader("Authorization", "Basic " + base64Encoded);
            httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
            httpPost.setEntity(new StringEntity("grant_type=client_credentials"));
            String rawAuthorization = getResponseBody(httpPost);
            Authenticated auth = jsonToAuthenticated(rawAuthorization);
            Log.e("resul", auth.token_type + "");
            // Applications should verify that the value associated with the
            // token_type key of the returned object is bearer
            if (auth != null && auth.token_type.equals("bearer")) {
                // Step 3: Authenticate API requests with bearer token
                //HttpGet httpGet = new HttpGet("https://api.twitter.com/1.1/friends/ids.json?stringify_ids=true&cursor=-1&screen_name=Brst Dev&user_id=109491200");
                HttpGet httpGet;
                httpGet = new HttpGet("https://api.twitter.com/1.1/followers/ids.json?stringify_ids=true&cursor=" + nextFollowers + "&screen_name=" + screenName + "&user_id=" + userId);
                // header with the value of Bearer <>
                httpGet.setHeader("Authorization", "Bearer " + auth.access_token);
                httpGet.setHeader("Content-Type", "application/json");
                // update the results with the body of the response
                results = getResponseBody(httpGet);
            }
        } catch (UnsupportedEncodingException ex) {
        } catch (IllegalStateException ex1) {
        }
        return results;
    }

    // convert a JSON authentication object into an Authenticated object
    private Authenticated jsonToAuthenticated(String rawAuthorization) {
        Authenticated auth = null;
        if (rawAuthorization != null && rawAuthorization.length() > 0) {
            try {
                Gson gson = new Gson();
                auth = gson.fromJson(rawAuthorization, Authenticated.class);
            } catch (IllegalStateException ex) {
                // just eat the exception
            }
        }
        return auth;
    }
    public class Authenticated {
        String token_type;
        String access_token;
    }

    public class checkLoginUser extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            {
                writingStatus="false";
                myRef1.child("Friends Only").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        //fetchUserFriends();
                        globalStoryList.clear();
                        for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                            Iterator i = postSnapshot.getChildren().iterator();
                            Log.e("finalsskeyy", postSnapshot.getKey());
                            for (int j = 0; j < friendList.size(); j++) {
                                Log.e("frindssid", friendList.get(j).getId() + "");
                                if (friendList.get(j).getId().equals(postSnapshot.getKey())) {
                                    String time = "";
                                    while (i.hasNext()) {
                                        DataSnapshot s = (DataSnapshot) i.next();
                                        String userId = s.child("hasWriting").getValue().toString();
                                        Log.e("userIdssss", userId);
                                        if(s.child("hasWriting").getValue().toString().equals("true"))
                                        {
                                            writingStatus = s.child("hasWriting").getValue().toString();
                                           // Toast.makeText(getActivity(),"true",Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if (writingStatus.equals("true")) {
                Toast.makeText(getActivity(), "Someone writing a story", Toast.LENGTH_LONG).show();
            } else {
                Calendar calander = Calendar.getInstance();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm");
                String time = simpleDateFormat.format(calander.getTime());
                Log.e("timee",time);
                String android_id = Settings.Secure.getString(getActivity().getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                Long tsLong = System.currentTimeMillis();
                String ts = tsLong.toString();
                Map<String, Object> taskMap = new HashMap<String, Object>();
                taskMap.put("description", "");
                taskMap.put("hasWriting", "true");
                taskMap.put("title", "");
                taskMap.put("topic", "");
                taskMap.put("userName", settings.getString("+name", ""));
                taskMap.put("profilePic", settings.getString("idtest", ""));
                taskMap.put("time", "");
                taskMap.put("androidId", android_id);
                taskMap.put("userId", idfire);
                taskMap.put("storyTime",time);
                taskMap.put("storyId",ts+"");
                myRef1.child("Friends Only").child(settings.getString("userId", "")).child(tsLong+"").setValue(taskMap);
                //ts = "Story_" + ts;
                // myRef.child("Write").child(ts).setValue(taskMap);
                Log.e("time", ts + "");
                //Toast.makeText(getActivity(),"open Activity "+loop,Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), PrivateWriteStoryActivity.class).putExtra("storyId", ts).putExtra("storyCount",globalStoryList.size()+"").putExtra("storyList",""+globalStoryList);
                startActivity(intent);
            }
        }
    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    private void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }
}
