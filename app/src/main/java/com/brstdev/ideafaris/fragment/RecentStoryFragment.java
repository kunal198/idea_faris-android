package com.brstdev.ideafaris.fragment;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.brstdev.ideafaris.GlobalApp;
import com.brstdev.ideafaris.MainAcivity;
import com.brstdev.ideafaris.R;
import com.brstdev.ideafaris.activity.TabIconText;
import com.brstdev.ideafaris.adapter.RecentStoryAdapter;
import com.brstdev.ideafaris.model.GlobalStory;
import com.brstdev.ideafaris.model.Like;
import com.brstdev.ideafaris.model.RecentStory;
import com.brstdev.ideafaris.model.TopRated;
import com.brstdev.ideafaris.model.WriteStoryFirebase;
import com.brstdev.ideafaris.utils.DividerItemDecoration;
import com.brstdev.ideafaris.utils.HttpHandler;
import com.brstdev.ideafaris.utils.IoUpdateCallback;
import com.brstdev.ideafaris.utils.MyTwitterApiClient;
import com.brstdev.ideafaris.utils.WebServiceHandler;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.FacebookSdk;
import com.facebook.HttpMethod;
import com.facebook.applinks.AppLinkData;
import com.facebook.internal.WebDialog;
import com.facebook.share.Sharer;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.ShareOpenGraphAction;
import com.facebook.share.model.ShareOpenGraphContent;
import com.facebook.share.model.ShareOpenGraphObject;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.AppInviteDialog;
import com.facebook.share.widget.ShareDialog;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Downloader;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

import bolts.AppLinks;
import io.fabric.sdk.android.Fabric;
import twitter4j.DirectMessage;
import twitter4j.HttpClient;
import twitter4j.HttpResponse;
import twitter4j.IDs;
import twitter4j.PagableResponseList;
import twitter4j.ResponseList;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;

import com.twitter.sdk.android.Twitter;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import twitter4j.IDs;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.DirectMessage;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
/**
 * Created by brst-pc16 on 9/14/16.
 */
public class RecentStoryFragment extends Fragment implements IoUpdateCallback {
    View rootView;
    private List<RecentStory> recentStories = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecentStoryAdapter mAdapter;
    private Toolbar mToolbar;
    private TextView toolbar_title;
    private DatabaseReference myRef1;
    private FirebaseDatabase mFirebaseDatabase;
    private ProgressDialog pd;
    SharedPreferences settings;
    public static final String PREFS_NAME = "login";
    private ArrayList<String> storyCount, disLikeCount;
    int count = 0;
    String idUser;
    public static ArrayList<String> like = new ArrayList<>();

    public static ArrayList<String> LikeMe = new ArrayList<>();
    public static ArrayList<String> disLikeMe = new ArrayList<>();


    static public HashMap<String, Long> likeCount = new HashMap<>();
    static public HashMap<String, Long> dislikeCount = new HashMap<>();

    private ArrayList<String> disLike = new ArrayList<>();
    static CallbackManager callbackManager;
    static ShareDialog shareDialog;

    int TWEETER_REQ_CODE = 11;
    private TwitterAuthClient client;
    private TextView txtNoStory;
    private IoUpdateCallback callback;
    private static final NavigableMap<Long, String> suffixes = new TreeMap<>();
    ProgressDialog dialog;

    static {
        suffixes.put(1_000L, "k");
        suffixes.put(1_000_000L, "M");
        suffixes.put(1_000_000_000L, "G");
        suffixes.put(1_000_000_000_000L, "T");
        suffixes.put(1_000_000_000_000_000L, "P");
        suffixes.put(1_000_000_000_000_000_000L, "E");
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);
        rootView = inflater.inflate(R.layout.recent_story_activity, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recentlist);
        toolbar_title = (TextView) rootView.findViewById(R.id.toolbar_title);
        txtNoStory = (TextView) rootView.findViewById(R.id.txtNoStory);
        callback = this;
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef1 = mFirebaseDatabase.getReference("StoryLine");
        storyCount = new ArrayList<String>();
        disLikeCount = new ArrayList<>();
        settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
        //taskMap.put("profilePic", settings.getString("idtest", ""));
        Log.e("pic",settings.getString("idtest",""));
        Log.e("pic",settings.getString("idfire", ""));
        Log.e("staussss", settings.getString("loginStausFacebook", ""));
        FacebookSdk.sdkInitialize(getActivity());
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        String appLinkUrl, previewImageUrl;
       // Toast.makeText(getActivity(),"call",Toast.LENGTH_SHORT).show();
      //  new GetContacts().execute();
        FacebookSdk.sdkInitialize(getActivity());
        Uri targetUrl =
                AppLinks.getTargetUrlFromInboundIntent(getActivity(), getActivity().getIntent());
        if (targetUrl != null) {
            Log.i("Activity", "App Link Target URL: " + targetUrl.toString());
        } else {
            AppLinkData.fetchDeferredAppLinkData(
                    getActivity(),
                    new AppLinkData.CompletionHandler() {
                        @Override
                        public void onDeferredAppLinkDataFetched(AppLinkData appLinkData) {
                            //process applink data
                            Log.e("linkk", appLinkData + "");
                        }
                    });
        }
        /*appLinkUrl = "https://fb.me/887879311348903";
        previewImageUrl = "https://firebasestorage.googleapis.com/v0/b/storyline-54d1f.appspot.com/o/logomain.png?alt=media&token=b1161dff-a739-4352-a1b0-b98c4b2cc9d8";
        if (AppInviteDialog.canShow()) {
            AppInviteContent content = new AppInviteContent.Builder()
                    .setApplinkUrl(appLinkUrl)
                    .setPreviewImageUrl(previewImageUrl)
                    .build();
            AppInviteDialog.show(this, content);
        }*/

        final String url ="https://api.twitter.com/1.1/friends/ids.json?stringify_ids=true&cursor=-1&screen_name=mahajan%20karan&user_id=109491200";
        new Thread() {
            @Override
            public void run() {
                //your code here
                try {
                    ConfigurationBuilder cb = new ConfigurationBuilder();
                    cb.setDebugEnabled(true)
                            .setOAuthConsumerKey("zJMhiUaLf6H7RNrFX1UaqEHdA")
                            .setOAuthConsumerSecret("9Scf01y0U6QlSDsBSEzSuEGTzj6Bf6GXy2Hh640D2nIGsoBusI")
                            .setOAuthAccessToken("3216318701-4KPi4hS1ylNsf97CNCvlTB7rYfE8rMhJ6gacv9c")
                            .setOAuthAccessTokenSecret("3lTBVwCP4kEDoeArPs6aQDYIl7j0SsorNzeTdojivJWuN");
                    TwitterFactory tf = new TwitterFactory(cb.build());
                    twitter4j.Twitter twitter = tf.getInstance();
                    // twitter4j.Twitter twitter = new TwitterFactory().getInstance();
                    long cursor = -1;
                    IDs ids;
                    System.out.println("Listing followers's ids.");
                   Log.e("fetchinnggg00", "fetchh");
                    //String Name = twitter.showUser("345017389").getName();
                  //  Log.e("alll","fff"+twitter.showUser("345017389").getName());
                   // ids = twitter.getFollowersIDs("brstdev", cursor);
                    do {
                        ids = twitter.getFollowersIDs("Brst-Dev", cursor);
                        for (long id : ids.getIDs()) {
                            System.out.println(id);
                            Log.e("idssssss", id + "");
                             Toast.makeText(getActivity(),""+id,Toast.LENGTH_SHORT).show();
                        }
                    } while ((cursor = ids.getNextCursor()) != 0);
                    // System.exit(0);
                } catch (TwitterException te) {
                    te.printStackTrace();
                    System.out.println("Failed to get followers' ids: " + te.getMessage());
                    //System.exit(-1);
                }

            }
        }.start();

        /*dialog=new ProgressDialog(getActivity());
        dialog.setMessage("loaa");
       dialog.show();
        new fetchTwitterId().execute();*/

        dialog=new ProgressDialog(getActivity());
        dialog.setMessage("Loading...");
        //dialog.show();
       // new fetchTwitterId().execute();
       /* final String url ="GET https://api.twitter.com/.1/friends/list.json";
        Log.e("PlayerUpdater", "Get: " + url);
        final ProgressDialog dialog=new ProgressDialog(getActivity());
        dialog.setMessage("loaa");
        dialog.show();*/

        final int occur=0;

        Log.e("PlayerUpdater", "Get: " + url);

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, url, (String) null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                // the esponse is already constructed as a JSONObject!
                Log.e("finallss", response.toString());
                try {
                    // response = response.getJSONObject();
               Log.e("responseee",response.toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("error",error.toString());
            }
        });
        Volley.newRequestQueue(getActivity()).add(jsonRequest);
        Log.e("deatilsss","df"+format(Long.parseLong("99")));

        for (int i = 0; i < 1000; i++) {
            storyCount.add("0");
            disLikeCount.add("0");
        }
        for (int i = 0; i < 1000; i++) {
            like.add("false");
            disLike.add("false");
        }
        idUser = settings.getString("idfire", "");
        String idfire = settings.getString("idfire", "");
        mAdapter = new RecentStoryAdapter(recentStories, getActivity(), idfire, "", "", this);
        getTotalLike();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);
        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        pd = new ProgressDialog(getActivity());
        pd.setMessage("Loading...");
        pd.show();
        prepareList();
        initToolbar();
        return rootView;
    }
    void initToolbar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayUseLogoEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar_title.setText("Recent Story");
        mToolbar.setNavigationIcon(R.drawable.arrow);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainAcivity.class);
                startActivity(intent);
            }
        });
    }

    private void prepareList() {
        //pd.show();
        Log.e("checkuser", "userr");
        myRef1.child("Recent_Story").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                recentStories.clear();
                pd.dismiss();
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    //Getting the data from snapshot
                    WriteStoryFirebase person = postSnapshot.getValue(WriteStoryFirebase.class);
                    Log.e("persn", "" + person.getDescription());
                    Log.e("persn", "" + person.getTopic());
                    Log.e("persn", "" + person.getTitle());
                    Log.e("deviceee", "fff" + person.getAndroidId());
                    Log.e("deviceee", "fff" + person.getLike());
                    Log.e("deviceee", "fff11" + person.getDisLike());
                    Log.e("deviceee final", "fff" + person.getStoryId());
                    try {
                        SimpleDateFormat format = new SimpleDateFormat("MMMM d,yyyy hh:mm a");
                        Calendar calander = Calendar.getInstance();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM d,yyyy hh:mm a");
                        String currentTime = simpleDateFormat.format(calander.getTime());
                        Date Date1 = format.parse(currentTime);
                        Date Date2 = format.parse(person.getTime());
                        long mills = Date1.getTime() - Date2.getTime();
                        Log.v("Data1", "" + Date1.getTime());
                        Log.v("Data2", "" + Date2.getTime());
                        int Hours = (int) (mills / (1000 * 60 * 60));
                        int Mins = (int) (mills / (1000 * 60)) % 60;
                        String diff = Hours + ":" + Mins; // updated value every1 second
                        Log.e("detailss", diff);
                        if (Mins == 0 && Hours < 1) {
                            person.setTime("Just Now");
                        } else if (Mins <= 59 && Hours < 1) {
                            person.setTime(String.valueOf(Mins) + " Minutes");
                        } else if (Hours >= 1 && Hours <= 24) {
                            person.setTime(String.valueOf(Hours) + " Hours");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.e("dislike", disLike.get(count));
                    RecentStory movie = new RecentStory(person.getTitle(), person.getUserName(), person.getTime(), person.getProfilePic(), person.getAndroidId(), format(Long.parseLong(person.getLike())), format(Long.parseLong(person.getDisLike())), person.getStoryId(), storyCount.get(count), like.get(count), disLikeCount.get(count), disLike.get(count), person.getDescription());
                    recentStories.add(movie);
                    ++count;
                }
                if (recentStories.size() == 0) {
                    txtNoStory.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                } else {
                    txtNoStory.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    Collections.reverse(recentStories);
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    void getTotalLike() {
        myRef1.child("Likes").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //recentStories.clear();
                //pd.dismiss();
                LikeMe.clear();
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    //Getting the data from snapshot
                    Iterator i = postSnapshot.getChildren().iterator();
                    Log.e("Key", postSnapshot.getKey());
                    likeCount.put(postSnapshot.getKey(), Long.valueOf(format(postSnapshot.getChildrenCount())));
                    //String isLikedByMe="false";
                    while (i.hasNext()) {
                        DataSnapshot s = (DataSnapshot) i.next();
                        String userId = s.child("userId").getValue().toString();
                        Log.e("userId", userId);
                        if (userId.equals(idUser)) {
                            // isLikedByMe="true";
                            LikeMe.add(s.child("storyId").getValue().toString());
                        }
                    }
                    // Log.e("isLikedByMe", isLikedByMe);
                    Like details = postSnapshot.getValue(Like.class);
                    storyCount.add(postSnapshot.getChildrenCount() + "");
                }
                Log.e("LikeMe", LikeMe + "");
                Collections.reverse(storyCount);
                //  Collections.reverse(like);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        myRef1.child("disLikes").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //recentStories.clear();
                //pd.dismiss();
                disLikeMe.clear();
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {

                    likeCount.put(postSnapshot.getKey(), Long.valueOf(format(postSnapshot.getChildrenCount())));

                    //Getting the data from snapshot
                    Iterator i = postSnapshot.getChildren().iterator();
                    while (i.hasNext()) {
                        DataSnapshot s = (DataSnapshot) i.next();
                        String userId = s.child("userId").getValue().toString();
                        Log.e("userId", userId);
                        if (userId.equals(idUser)) {
                            disLikeMe.add(s.child("storyId").getValue().toString());
                        }
                        //disLike.add("true");
                        // break;
                        //}
                        //else
                        //disLike.add("false");
                    }
                    Like details = postSnapshot.getValue(Like.class);
                    Log.e("finalss", postSnapshot.getChildrenCount() + "");
                    disLikeCount.add(postSnapshot.getChildrenCount() + "");
                }
                Log.e("disLikeMe", disLikeMe + "");
                Collections.reverse(disLikeCount);
                Collections.reverse(disLike);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

    }

    public void setShare(String title, String description) {

    }
    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Make sure that the loginButton hears the result from any
        // Activity that it triggered.
//     client.onActivityResult(requestCode, resultCode, data);


       // Log.d("twitter", "enter" + RESULT_OK);
    }*/
    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getActivity().getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }
    @Override
    public void update(String title,String description) {
        Log.e("titlee", "title");
        Uri uri=null;
        try {
            uri  = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://"  + getResources().getResourcePackageName(R.mipmap.logomain) + '/' + getResources().getResourceTypeName(R.mipmap.logomain) + '/' + String.valueOf(R.mipmap.logomain));
            //uri = Uri.parse("android.resource://com.brstdev.ideafaris/drawable/c4.png");
            InputStream stream = getActivity().getContentResolver().openInputStream(uri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (settings.getString("loginStausFacebook", "").equals("true")) {
            if (ShareDialog.canShow(ShareLinkContent.class)) {
                ShareLinkContent linkContent = new ShareLinkContent.Builder()
                        .setContentTitle(title)
                        .setContentDescription(
                                description)
                        .setContentUrl(Uri.parse("https://play.google.com/store?hl=en"))
                        .build();
                shareDialog.show(linkContent);
            }}else {
                if (appInstalledOrNot("com.twitter.android")) {
                   // Uri uri = Uri.parse("android.resource://your.package.here/drawable/image_name");
                    TwitterAuthConfig authConfig = new TwitterAuthConfig("zJMhiUaLf6H7RNrFX1UaqEHdA", "9Scf01y0U6QlSDsBSEzSuEGTzj6Bf6GXy2Hh640D2nIGsoBusI");
                    Fabric.with(getActivity(), new TwitterCore(authConfig), new TweetComposer());
                    TweetComposer.Builder builder = new TweetComposer.Builder(getActivity())
                            .text(title + "\n\n" + description)
                     .image(uri);
                    builder.show();
                } else {
                    Toast.makeText(getActivity(), "Please install twitter app first", Toast.LENGTH_SHORT).show();
                }
        }
    }

    public static String format(long value) {
        //Long.MIN_VALUE == -Long.MIN_VALUE so we need an adjustment here
        if (value == Long.MIN_VALUE) return format(Long.MIN_VALUE + 1);
        if (value < 0) return "-" + format(-value);
        if (value < 1000) return Long.toString(value); //deal with easy case
        Map.Entry<Long, String> e = suffixes.floorEntry(value);
        Long divideBy = e.getKey();
        String suffix = e.getValue();
        long truncated = value / (divideBy / 10); //the number part of the output times 10
        boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
        return hasDecimal ? (truncated / 10d) + suffix : (truncated / 10) + suffix;
    }

}


