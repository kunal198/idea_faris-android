package com.brstdev.ideafaris.utils;

import android.util.Base64;
import android.util.Log;

import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;

import java.io.*;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Gagan on 8/18/15.
 */
public class WebServiceHandler
{

    //*****************************************************************************************************
    //****************************************   Twitter   ************************************************
    //*****************************************************************************************************
    private String getResponseBody(HttpRequestBase request)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            DefaultHttpClient httpClient = new DefaultHttpClient(new BasicHttpParams());
            HttpResponse response = httpClient.execute(request);
            int statusCode = response.getStatusLine().getStatusCode();
            String reason = response.getStatusLine().getReasonPhrase();
            if (statusCode == 200)
            {
                HttpEntity entity = response.getEntity();
                InputStream inputStream = entity.getContent();

                BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
                String line = null;
                while ((line = bReader.readLine()) != null)
                {
                    sb.append(line);
                }
            }
            else
            {
                sb.append(reason);
            }
        }
        catch (UnsupportedEncodingException ex)
        {
        }
        catch (ClientProtocolException ex1)
        {
        }
        catch (IOException ex2)
        {
        }
        return sb.toString();
    }


    public String getTwitterStream(String screenName,String userId)
    {
        String results = null;
        // Step 1: Encode consumer key and secret
        try
        {
            // URL encode the consumer key and secret
            String urlApiKey = URLEncoder.encode("zJMhiUaLf6H7RNrFX1UaqEHdA", "UTF-8");
            String urlApiSecret = URLEncoder.encode("9Scf01y0U6QlSDsBSEzSuEGTzj6Bf6GXy2Hh640D2nIGsoBusI", "UTF-8");
            // Concatenate the encoded consumer key, a colon character, and the
            // encoded consumer secret
            String combined = urlApiKey + ":" + urlApiSecret;
            // Base64 encode the string

            String base64Encoded = Base64.encodeToString(combined.getBytes(), Base64.NO_WRAP);
            // Step 2: Obtain a bearer token

            HttpPost httpPost = new HttpPost("https://api.twitter.com/oauth2/token");
            httpPost.setHeader("Authorization", "Basic " + base64Encoded);
            httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
            httpPost.setEntity(new StringEntity("grant_type=client_credentials"));
            String rawAuthorization = getResponseBody(httpPost);
            Authenticated auth = jsonToAuthenticated(rawAuthorization);
            Log.e("resul", auth.token_type + "");
            // Applications should verify that the value associated with the
            // token_type key of the returned object is bearer
            if (auth != null && auth.token_type.equals("bearer"))
            {
                // Step 3: Authenticate API requests with bearer token
                //HttpGet httpGet = new HttpGet("https://api.twitter.com/1.1/friends/ids.json?stringify_ids=true&cursor=-1&screen_name=Brst Dev&user_id=109491200");
                HttpGet httpGet = new HttpGet("https://api.twitter.com/1.1/friends/ids.json?stringify_ids=true&cursor=-1&screen_name="+screenName+"&user_id="+userId);

                //HttpGet httpGet = new HttpGet("https://api.twitter.com/1.1/friends/ids.json?stringify_ids=true&cursor=-1&screen_name=mahajan%20karan&user_id=109491200");
//                HttpGet httpGet = new HttpGet(MyConstant.TwitterStreamURL + screenName);

                // construct a normal HTTPS request and include an Authorization
                // header with the value of Bearer <>
                httpGet.setHeader("Authorization", "Bearer " + auth.access_token);
                httpGet.setHeader("Content-Type", "application/json");
                // update the results with the body of the response
                results = getResponseBody(httpGet);
            }
        }
        catch (UnsupportedEncodingException ex)
        {
        }
        catch (IllegalStateException ex1)
        {
        }
        return results;
    }
    // convert a JSON authentication object into an Authenticated object
    private Authenticated jsonToAuthenticated(String rawAuthorization)
    {
        Authenticated auth = null;
        if (rawAuthorization != null && rawAuthorization.length() > 0)
        {
            try
            {
                Gson gson = new Gson();
                auth = gson.fromJson(rawAuthorization, Authenticated.class);
            }
            catch (IllegalStateException ex)
            {
                // just eat the exception
            }
        }
        return auth;
    }

    public class Authenticated {
        String token_type;
        String access_token;
    }


}