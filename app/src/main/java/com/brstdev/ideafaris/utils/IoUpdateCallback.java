package com.brstdev.ideafaris.utils;

/**
 * Created by brst-pc80 on 11/8/16.
 */
public interface IoUpdateCallback {

    void update(String title,String description);

}
