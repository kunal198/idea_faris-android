package com.brstdev.ideafaris.model;

/**
 * Created by brst-pc16 on 9/13/16.
 */
public class RecentStory {
    private String title;
    private String genre;
    private String year;
    private String andoridId;
    private String like;
    private String storyCount;
    private String likeStatus;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private String description;

    public String getDislikeCount() {
        return dislikeCount;
    }

    public void setDislikeCount(String dislikeCount) {
        this.dislikeCount = dislikeCount;
    }

    public String getDislikeStatus() {
        return dislikeStatus;
    }

    public void setDislikeStatus(String dislikeStatus) {
        this.dislikeStatus = dislikeStatus;
    }

    private String dislikeCount;
    private String dislikeStatus;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    private String userId;

    public String getLikeStatus() {
        return likeStatus;
    }

    public void setLikeStatus(String likeStatus) {
        this.likeStatus = likeStatus;
    }

    public String getStoryCount() {
        return storyCount;
    }

    public void setStoryCount(String storyCount) {
        this.storyCount = storyCount;
    }

    public String getDisLike() {
        return disLike;
    }

    public void setDisLike(String disLike) {
        this.disLike = disLike;
    }

    public String getStoryId() {
        return storyId;
    }

    public void setStoryId(String storyId) {
        this.storyId = storyId;
    }

    public String getLike() {
        return like;
    }

    public void setLike(String like) {
        this.like = like;
    }

    public String getAndoridId() {
        return andoridId;
    }

    public void setAndoridId(String andoridId) {
        this.andoridId = andoridId;
    }

    private String disLike;
    private String storyId;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    private String imageUrl;
    public RecentStory() {
    }
    public RecentStory(String title, String genre, String year,String imageUrl,String androidId,String like,String disLike,String storyId,String storyCount,String likeStatus,String disLikeCount,String dislikeStatus,String description) {
        this.title = title;
        this.genre = genre;
        this.year = year;
        this.imageUrl=imageUrl;
        this.andoridId=androidId;
        this.like=like;
        this.disLike=disLike;
        this.storyId=storyId;
        this.storyCount=storyCount;
        this.likeStatus=likeStatus;
        this.dislikeCount=disLikeCount;
        this.dislikeStatus=dislikeStatus;
        this.description=description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
