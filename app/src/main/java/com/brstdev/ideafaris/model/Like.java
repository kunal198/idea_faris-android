package com.brstdev.ideafaris.model;

/**
 * Created by brst-pc16 on 9/29/16.
 */
public class Like {
    public String getLike() {
        return like;
    }

    public void setLike(String like) {
        this.like = like;
    }

    public String like;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String userId;
    public Like() {
    }
    public Like(String like,String userId) {
        this.like = like;
        this.userId=userId;

    }

}
