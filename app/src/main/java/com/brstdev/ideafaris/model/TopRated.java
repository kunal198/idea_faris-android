package com.brstdev.ideafaris.model;

/**
 * Created by brst-pc16 on 9/12/16.
 */
public class TopRated {
    private String title;
    private String genre;
    private String year;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private String description;

    public String getStoryId() {
        return storyId;
    }

    public void setStoryId(String storyId) {
        this.storyId = storyId;
    }

    private String storyId;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    String imageUrl;
    public TopRated() {
    }

    public TopRated(String imageUrl, String genre, String year,String title,String storyId,String description) {
        this.title = title;
        this.genre = genre;
        this.year = year;
        this.imageUrl=imageUrl;
        this.storyId=storyId;
        this.description=description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}

