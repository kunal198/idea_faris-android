package com.brstdev.ideafaris.model;

/**
 * Created by brst-pc16 on 9/13/16.
 */
public class FriendList {
    private String freindname;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    private Boolean status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    private String imageUrl;
    public String getFreindemail() {
        return freindemail;
    }

    public void setFreindemail(String freindemail) {
        this.freindemail = freindemail;
    }

    public String getFreindname() {
        return freindname;
    }

    public void setFreindname(String freindname) {
        this.freindname = freindname;
    }

    private String freindemail;

    public FriendList() {
    }

    public FriendList(String freindname, String freindemail,String imageUrl,Boolean status ) {
        this.freindname = freindname;
        this.freindemail = freindemail;
        this.imageUrl=imageUrl;
        this.status=status;
    }
}