package com.brstdev.ideafaris.model;

/**
 * Created by brst-pc16 on 9/29/16.
 */
public class WriteStoryFirebase  {
    String description;
    String hasWriting;
    String userName;
    String profilePic;
    String time;
    String storyCount;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    String userId;

    public String getStoryCount() {
        return storyCount;
    }

    public void setStoryCount(String storyCount) {
        this.storyCount = storyCount;
    }

    public String getAndroidId() {
        return androidId;
    }

    public void setAndroidId(String androidId) {
        this.androidId = androidId;
    }

    String androidId;

    public String getLike() {
        return like;
    }

    public void setLike(String like) {
        this.like = like;
    }

    public String getDisLike() {
        return disLike;
    }

    public void setDisLike(String disLike) {
        this.disLike = disLike;
    }

    public String getStoryId() {
        return storyId;
    }

    public void setStoryId(String storyId) {
        this.storyId = storyId;
    }

    String like;
    String disLike;
    String storyId;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public WriteStoryFirebase() {
    }
    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHasWriting() {
        return hasWriting;
    }

    public void setHasWriting(String hasWriting) {
        this.hasWriting = hasWriting;
    }

    String title;
    String topic;
   /* public WriteStoryFirebase(String name, String title) {
        this.name = name;
        this.title = title;
    }

    public WriteStoryFirebase() {
    }*/
    public WriteStoryFirebase(String description, String hasWriting, String title,String topic,String userName,String profilePic,String time,String androidId,String like,String disLike,String storyId,String userId) {
        this.description = description;
        this.hasWriting = hasWriting;
        this.title = title;
        this.topic = topic;
        this.userName=userName;
        this.profilePic=profilePic;
        this.time=time;
        this.androidId=androidId;
        this.like=like;
        this.disLike=disLike;
        this.storyId=storyId;
        this.userId=userId;
    }
}
