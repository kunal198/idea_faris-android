package com.brstdev.ideafaris.model;

/**
 * Created by brst-pc16 on 9/13/16.
 */
public class History {
    private String title;
    private String genre;
    private String year;

    public String getLike() {
        return like;
    }

    public void setLike(String like) {
        this.like = like;
    }

    private String like;

    public String getDisLike() {
        return disLike;
    }

    public void setDisLike(String disLike) {
        this.disLike = disLike;
    }

    private String disLike;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    private String time;

    public String getStoryId() {
        return storyId;
    }

    public void setStoryId(String storyId) {
        this.storyId = storyId;
    }

    private String storyId;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    String imageUrl;
    public History() {
    }

    public History(String imageUrl, String genre, String year,String title,String storyId,String time,String like,String disLike) {
        this.title = title;
        this.genre = genre;
        this.year = year;
        this.imageUrl=imageUrl;
        this.storyId=storyId;
        this.time=time;
        this.like=like;
        this.disLike=disLike;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }}